$(function () {
    clearAllSession();

});

function clearAllSession () {
    sessionStorage.removeItem("token");
    sessionStorage.removeItem("refresh_token");

    localStorage.setItem("remember_me", false);
}

$("#frm-login").submit(async function (e) {
    e.preventDefault();
    Pace.restart();
    var username = $("#username").val();
    var password = $("#password").val();
    var remember_me = $("#remember_me").prop('checked');

    var data = {
        'username': username,
        'password': password
    }

    const options = {
        method: "POST",
        url: url + "/auth/token",
        headers: {
            "content-Type": "application/json"
        },
        data: JSON.stringify(data)
    }
    try {
        const result = await axios(options);
        const token = result.data.access_token;
        const user = await validateCurrentUser(token);
        var refresh_token = result.data.refresh_token;
        var expires_in = result.data.expires_in;
        var date = new Date();

        localStorage.setItem("token", token);
        localStorage.setItem("refresh_token", refresh_token);

        sessionStorage.setItem("token", token);
        sessionStorage.setItem("refresh_token", refresh_token);

        if (remember_me) {
            localStorage.setItem("remember_me", true);
        } else {
            localStorage.setItem("remember_me", false);
        }

        localStorage.setItem("user", user.userName);
        window.location.assign(url + "/admin/dashboard");

    } catch (error) {
        $('.message-error').text('Invalid username or password.');
        throw error;
    }
})

async function validateCurrentUser (token) {
    const options = {
        method: 'GET',
        url: url + "/api/users/current",
        headers: {
            'content-Type': 'application/json',
            'authorization': 'Bearer ' + token
        }
    }
    try {
        const result = await axios(options).catch(function (error) {
            throw error.response;
        });
        return result.data;
    } catch (error) {
        $('.message-error').text('Invalid username or password.');
        throw error;
    }
}