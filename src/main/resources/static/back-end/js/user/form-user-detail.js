$(function () {
  loadData();
  loadMemberData();
});

async function loadData () {
  var id = $('#id').val();
  
  if (id != null && id != "") {
    var result = await getAllData("api/users/" + id);
    $('#username').text(result.userName);
    $('#email').text(result.email);
    $('#mobile').text(result.mobile);
    $('#display-name').text(result.displayName);
    $('#user-profile-picture').prop('src', result.photoUrl);
    $('#card-number').text(result.cardNumber);
  }
}

async function loadMemberData () {
  var id = $('#id').val();

  if (id != null && id != "") {
    var result = await getAllData("api/member/" + id);
    $('#username').text(result.khmerFullName);
    $('#khmerFullName').text(result.khmerFullName);
    $('#englishFullName').text(result.englishFullName);
    $('#idCardNo').text(result.idCardNo);
    $('#sex').text(result.sex);
    $('#dateOfBirth').text(result.dateOfBirth);
    $('#addressLIne').text(result.address.addressLIne);
    $('#village').text(result.address.village);
    $('#commune').text(result.address.commune);
    $('#district').text(result.address.district);
    $('#province').text(result.address.province);
    $('#idCardImageFront').prop('src', result.idCardImageFront);
    $('#idCardImageBack').prop('src', result.idCardImageBack);
  }
}
