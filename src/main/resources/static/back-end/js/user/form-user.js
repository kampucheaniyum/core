$(function () {
  loadData();
});

$('#btn-list').on('click', function () {
  window.location.assign(url + "/admin/user");
})

$('#btn-clear').on('click', function () {
  clearValuesByIds(["username", "email", "mobile", "role", "password", "confirm_password"]);
  $('#self_service_user').prop('checked', false);
  $('.form-group').removeClass("has-error");
  $('.form-group span.help-block').text('');
})

async function loadData () {
  var id = $('#id').val();

  $('.password-star').removeClass("hidden");
  
  if (id != null && id != "") {
    var result = await getAllData("api/users/" + id);
    $('#username').val(result.userName);
    $('#email').val(result.email);
    $('#mobile').val(result.mobile);
    $('#role').val(result.role);
    $('.password-star').addClass("hidden");
    $('#username').attr('readonly', true);
  }
}

$('#frm-user').submit(async function (e) {
  e.preventDefault();

  var id = $('#id').val();
  var username = $('#username').val();
  var email = $('#email').val();
  var mobile = $('#mobile').val();
  var role = $('#role').val();
  var password = $('#password').val();
  var confirm_password = $('#confirm_password').val();
  // start validation

  var nullValidation = null;
  if (id != null && id != "") {
    if (password == "" && confirm_password == "") {
      nullValidation = validationNullByIds(["username", "email", "role"]);
    } else {
      nullValidation = validationNullByIds(["username", "email", "role", "password", "confirm_password"]);
    }
  } else {
    nullValidation = validationNullByIds(["username", "email", "role", "password", "confirm_password"]);
  }
  if (nullValidation) {
    return false;
  }

  if (password != confirm_password) {
    $('#frm-group-confirm_password').addClass("has-error");
    $('#frm-group-confirm_password span.help-block').html('not match with password');
    return false;
  } else {
    $('#frm-group-confirm_password').removeClass("has-error");
    $('#frm-group-confirm_password span.help-block').html('');
  }
  // end validation

  var data = {
    "userName": username,
    "email": email,
    "role": role,
    "password": password,
    "mobile": mobile
  }

  var path = "/api/users";
  var method = "POST";
  var title = "Add Record"

  if (id != null && id != "") {
    //data = { "id": id, "name": name }
    path = "/api/users/" + id;
    method = "PUT";
    title = "Update Record";
  }
  var token = await getToken();
  $.confirm({
    icon: 'fa fa-question',
    theme: 'supervan',
    closeIcon: true,
    animation: 'scale',
    type: 'blue',
    title: title,
    content: 'Are you sure to continue?',
    buttons: {
      Yes: function () {
        $.ajax({
          url: url + path,
          method: method,
          beforeSend: function (xhr) {
            xhr.setRequestHeader("Content-Type", "application/json");
            xhr.setRequestHeader("authorization", "Bearer " + token);
          },
          data: JSON.stringify(data),
          success: function (json) {
            $('#btn-list').click();
          },
          error: function (error) {
            var json = error.responseJSON;
            if (error.status == 422) {
              var errors = json.errors;

              var user_name_errors = [];
              var email_errors = [];
              $.each(errors, function (key, value) {
                var userNameMess = "userName| ";
                var emailMess = "email| ";
                if (value.indexOf(userNameMess) == 0) {
                  user_name_errors.push(value.substring(userNameMess.length));
                }
                if (value.indexOf(emailMess) == 0) {
                  email_errors.push(value.substring(emailMess.length));
                }
              })

              if (user_name_errors.length > 0) {
                $('#frm-group-username').addClass("has-error");
                $('#frm-group-username span.help-block').text(user_name_errors[0]);
              }

              if (email_errors.length > 0) {
                $('#frm-group-email').addClass("has-error");
                $('#frm-group-email span.help-block').text(email_errors[0]);
              }

            } else {
              console.log(json.message);
            }
          }
        });
      },
      No: function () {

      }
    }
  });

})