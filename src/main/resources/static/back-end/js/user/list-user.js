$(async function () {
    await gets();

});

async function gets (page = null, data = null) {
    Pace.restart();
    const limit = $('#show-qty').val();

    page == null ? 0 : page;
    var result = await getAllData("api/users?page=" + page + "&size=" + limit, data);
    getPagination(result, this);
    var html = "";
    var no = result.number * result.size;
    $.each(result.content, function (k, v) {
        html += '<tr>'
            + '<td>' + (k + no + 1) + '</td>'
            + '<td>' + v.userName + '</td>'
            + '<td>' + v.email + '</td>'
            + '<td>' + v.mobile + '</td>'
            + '<td>' + v.role + '</td>'
//            + '<td>' + '<a onclick="previewQR(\'' + v.qrCode + '\')"><i class="fa fa-fw fa fa-eye"></i></a>' + '</td>'
            + '<td class="text-center">'
            + '<button type="button" onclick="editUser(' + v.id + ')" class="btn btn-xs btn-warning btn-update-user"><i class="fa fa-fw fa-pencil-square-o"></i></button>&nbsp;&nbsp;'
            + '<button type="button" onclick="deleteUser(' + v.id + ')" class="btn btn-xs btn-danger btn-delete-user"><i class="fa fa-fw fa fa-trash-o"></i></button>'
            + '</td>'
            + '</tr>';
    })
    $('#table-data tbody').html(html);
    permissionCheck('UPDATE_USER', 'btn-update-user');
    permissionCheck('DELETE_USER', 'btn-delete-user');
}

function previewQR (qrCodeUrl) {
    if (qrCodeUrl == null || qrCodeUrl === "")
        return false;

    $('#qrcode img').prop('src', '');

    const qrcode = new QRCode(document.getElementById('qrcode'), {
      text: qrCodeUrl,
      width: 400,
      height: 400,
      colorDark : '#000',
      colorLight : '#fff',
      correctLevel : QRCode.CorrectLevel.H
    });

    $('#modal-preview-qr').modal('toggle');
}

function editUser (id) {
    window.location.assign(url + "/admin/user/" + id);
}

async function deleteUser (id) {
    var token = await getToken();
    $.confirm({
        icon: 'fa fa-question',
        theme: 'supervan',
        closeIcon: true,
        animation: 'scale',
        type: 'blue',
        title: 'Delete Record',
        content: 'Are you sure to delete data?',
        autoClose: 'Cancel|5000',
        buttons: {
            deleteUser: {
                text: 'Delete',
                action: function () {
                    $.ajax({
                        url: url + "/api/users/" + id,
                        method: "DELETE",
                        beforeSend: function (xhr) {
                            xhr.setRequestHeader("Content-Type", "application/json");
                            xhr.setRequestHeader("authorization", "Bearer " + token);
                        },
                        success: function (json) {
                            gets();
                        },
                        error: function (error) {
                            var json = error.responseJSON;
                            if (error.status == 422) {
                                $.alert({
                                    title: '<p style="color:red;">Alert!</p>',
                                    content: json.errors[0],
                                });
                            } else {
                                console.log(json.message);
                            }
                        }
                    });
                }
            },
            Cancel: function () {
            }
        }
    });
}

$('#frm-search').on('submit', async function (e) {
    e.preventDefault();
    const data = $(this).serializeFormJSON();
    await gets(0, data);
})

$('#export-pdf').on('click', function () {
    exportPdf('table-data', 'List User');
})

$('#export-excel').on('click', function () {
    exportExcel('table-data', 'List User');
})

$('#show-qty').on('change', async function () {
    const data = $('#frm-search').serializeFormJSON();
    await gets(0, data);
})