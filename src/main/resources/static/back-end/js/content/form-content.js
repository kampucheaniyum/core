const content = CKEDITOR.replace('content', {
    extraPlugins: 'uploadimage,image2',
    removePlugins: 'image',
    height: 400
});
CKFinder.setupCKEditor(content);

$(function () {
    getById();
});

$('#btn-list').on('click', function () {
    window.location.assign(url + "/admin/content");
})

$('#btn-clear').on('click', function () {
    clearValuesByIds(["title"]);
    CKEDITOR.instances.content.setData('');
})

async function getById () {
    var id = $('#id').val();

      var category = await getAllData("api/category");
      $.each(category, function (k, v) {
        $('#category').append('<option value="' + v.id + '">' + v.name + '</option>');
      })

    if (id != null && id != "") {
        var result = await getAllData("api/content/" + id);
        $('#title').val(result.title);
        $('#category').val(result.categoryId);
        $('#mediaUrl').val(result.mediaUrl);
        $('#contentType').val(result.contentType);
        CKEDITOR.instances.content.setData(result.content);
    }
}

$('#frm-submit').submit(async function (e) {
    e.preventDefault();

    const id = $('#id').val();
    const title = $('#title').val();
    const categoryId = $('#category').val();
    const content = CKEDITOR.instances.content.getData();
    const mediaUrl = $('#mediaUrl').val();
    const contentType = $('#contentType').val();

    const data = {
        categoryId: categoryId,
        title: title,
        content: content,
        mediaUrl: mediaUrl,
        contentType: contentType
    }

    if (id != null && id != "") {
        try {
            await submitData('PUT', 'api/content/' + id, data);
            $('#btn-list').click();
        } catch (error) {
            console.log(error.data.message);
            $('#error-message').text(error.data.errors[0]);
        }
    } else {
        try {
            await submitData('POST', 'api/content', data);
            $('#btn-list').click();
        } catch (error) {
            console.log(error.data.message);
            $('#error-message').text(error.data.errors[0]);
        }
    }
})