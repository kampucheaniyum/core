$(function () {
    gets();
    $('#from').datepicker({
        autoclose: true,
        format: dateFormat,
        todayHighlight: true,
        forceParse: true
    })

    $('#to').datepicker({
        autoclose: true,
        format: dateFormat,
        todayHighlight: true,
        forceParse: true
    })

    getCategory();
});

async function getCategory() {
      var category = await getAllData("api/category");
      $.each(category, function (k, v) {
        $('#categoryId').append('<option value="' + v.id + '">' + v.name + '</option>');
      })
}

async function gets (page = null) {
    Pace.restart();
    page == null ? 0 : page;
    var data = $('#frm-search').serializeFormJSON();
    var limit = $('#show-qty').val();
    var result = await getAllData("api/content?sort=priority,desc&page=" + page + "&size=" + limit, data);
    var html = "";
    var no = result.number * result.size;
    $.each(result.content, function (k, v) {
        html += '<tr>'
            + '<td>' + (k + no + 1) + '</td>'
            + '<td>' + v.categoryName + '</td>'
            + '<td>' + v.contentType + '</td>'
            + '<td>' + v.title + '</td>'
            + '<td><a href="'+ v.mediaUrl +'">' + v.mediaUrl + '</a></td>'
            + '<td>' + v.priority + '</td>'
            + '<td class="text-center">'
                    + '<button type="button" onclick="plus(' + v.id + ')" class="btn btn-xs btn-success btn-update-news"><i class="fa fa-fw fa-arrow-up"></i></button>&nbsp;&nbsp;'
                    + '<button type="button" onclick="minus(' + v.id + ')" class="btn btn-xs btn-primary btn-delete-news"><i class="fa fa-fw fa fa-arrow-down"></i></button>'
            + '</td>'
            + '<td>' + v.createdAt + '</td>'
            + '<td class="text-center">'
                + '<button type="button" onclick="edit(' + v.id + ')" class="btn btn-xs btn-warning btn-update-news"><i class="fa fa-fw fa-pencil-square-o"></i></button>&nbsp;&nbsp;'
                + '<button type="button" onclick="deleteAction(' + v.id + ')" class="btn btn-xs btn-danger btn-delete-news"><i class="fa fa-fw fa fa-trash-o"></i></button>'
            + '</td>'
            + '</tr>';
    })
    $('#table-data tbody').html(html);
    $('#pagination').twbsPagination({
        initiateStartPageClick: false,
        totalPages: result.totalPages,
        visiblePages: 10,
        onPageClick: async function (event, page) {
            await gets(page-1);
        }
    });
}

function edit (id) {
    window.location.assign(url + "/admin/content/" + id);
}

async function plus (id) {
    const data = {};
    await requestAPI ('POST', 'api/content/' + id + '/priority?command=increase', data, false)
    await gets();
}

async function minus (id) {
    const data = {};
    await requestAPI ('POST', 'api/content/' + id + '/priority?command=decrease', data, false)
    await gets();
}

async function deleteAction (id) {
    try {
        const del = await deleteData("api/content/delete/" + id);
        if (del)
            await gets();
    } catch (error) {
        console.log(error);
    }
}

$('#frm-search').on('submit', async function (e) {
    e.preventDefault();
    $('#pagination').twbsPagination('destroy');
    await gets(0);
})

$('#export-pdf').on('click', function () {
    Pace.restart();
    exportPdf('table-data', 'List Content');
})

$('#export-excel').on('click', function () {
    Pace.restart();
    exportExcel('table-data', 'List Content');
})

$('#show-qty').on('change', async function () {
    $('#pagination').twbsPagination('destroy');
    await gets(0);
})
