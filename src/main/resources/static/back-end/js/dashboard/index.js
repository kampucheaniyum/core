$(function () {
    var result = getAllData("api/member/statistic")
            .then(response=> {
                $('#activeMember').text(response.approved);
                $('#pendingMember').text(response.pending);
                $('#rejectMember').text(response.rejected);
            });
})
