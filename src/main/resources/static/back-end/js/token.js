$(async function () {
    await checkSession();
});

async function checkSession () {
    var token = localStorage.getItem("token");
    var remember = localStorage.getItem("remember_me");
    var refresh_token = localStorage.getItem("refresh_token");
    var date = new Date();
    if (remember === "true") {
        await getRefreshToken(refresh_token);
    } else {

    }
}

async function getToken () {
    var token = localStorage.getItem("token");
    var expire = localStorage.getItem("expire");
    var remember = localStorage.getItem("remember_me");
    var refresh_token = localStorage.getItem("refresh_token");
    var date = new Date();
    if (remember === "true") {
        var new_token = await getRefreshToken(refresh_token);
        return new_token;
    } else {
        return token;
    }
}

function getRefreshToken (old_refresh_token) {
    return new Promise(resolve => {
        $.ajax({
            url: url + "/auth/token",
            method: "POST",
            beforeSend: function (xhr) {
                xhr.setRequestHeader("content-Type", "application/json");
            },
            data: {
                "refresh_token": old_refresh_token,
                "grant_type": "refresh_token"
            },
            success: function (json) {
                var token = json.access_token;
                var refresh_token = json.refresh_token;

                localStorage.setItem("token", token);
                localStorage.setItem("refresh_token", refresh_token);

                sessionStorage.setItem("token", token);
                sessionStorage.setItem("refresh_token", refresh_token);
                var token = localStorage.getItem("token");
                resolve(token);
            },
            error: function (error) {
                window.location.replace(url + "/admin/logout");
            }
        });
    })

}