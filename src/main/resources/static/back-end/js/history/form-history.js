const content = CKEDITOR.replace('content', {
    extraPlugins: 'uploadimage,image2',
    removePlugins: 'image',
    height: 400
});
CKFinder.setupCKEditor(content);

$(async function () {
    await getById();
});

$('#btn-list').on('click', function () {
    window.location.assign(url + "/admin/history");
})

$('#btn-clear').on('click', function () {
    clearValuesByIds(["content"]);
})

async function getById () {
    var result = await getAllData("api/organization/history");
    CKEDITOR.instances.content.setData(result.content);
}

$('#frm-submit').submit(async function (e) {
    e.preventDefault();

    const content = CKEDITOR.instances.content.getData();

    var data = {
        "content": content,
        "detailType": "HISTORY"
    };
    try {
        await submitData('PUT', 'api/organization', data);
        $('#btn-list').click();
    } catch (error) {
        console.log(error);
    }
})