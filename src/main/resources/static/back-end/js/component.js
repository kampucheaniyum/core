async function getAllData (apiUrl, query = null) {
    const token = await getToken();
    const options = {
        method: 'GET',
        url: url + "/" + apiUrl,
        headers: {
            'content-Type': 'application/json',
            'authorization': 'Bearer ' + token
        },
        params: query
    }
    const result = await axios(options).catch(function (error) {
        if (error.response.status == 401) {
            window.location.replace(url + "/admin/logout");
        }
        throw error.response;
    });
    return result.data
}

async function deleteData (apiUrl) {
    return new Promise(resolve => {
        $.confirm({
            icon: 'fa fa-question',
            theme: 'supervan',
            closeIcon: true,
            animation: 'scale',
            type: 'blue',
            title: 'Delete Record',
            content: 'Are you sure to delete data?',
            autoClose: 'Cancel|5000',
            buttons: {
                deleteUser: {
                    text: 'Delete',
                    action: async function () {
                        const token = await getToken();
                        const options = {
                            method: 'DELETE',
                            url: url + "/" + apiUrl,
                            headers: {
                                'content-Type': 'application/json',
                                'authorization': 'Bearer ' + token
                            }
                        }
                        try {
                            await axios(options).catch(function (error) {
                                if (error.response.status == 401) {
                                    window.location.replace(url + "/admin/logout");
                                }
                                throw error.response;
                            });
                            resolve(true);
                        } catch (error) {
                            console.log(error)
                            return error
                        }
                    }
                },
                Cancel: function () {
                }
            }
        });
    })
}

async function submitData (method, apiUrl, data, isFormData = null) {
    return new Promise(resolve => {
        $.confirm({
            icon: 'fa fa-question',
            theme: 'supervan',
            closeIcon: true,
            animation: 'scale',
            type: 'blue',
            title: 'Submit data',
            content: 'Are you sure to contiute?',
            buttons: {
                Yes: async function () {
                    const token = await getToken();

                    let contentType = "application/json"
                    if (isFormData)
                        contentType = "multipart/form-data";

                    const options = {
                        method: method,
                        url: url + "/" + apiUrl,
                        headers: {
                            'content-Type': contentType,
                            'authorization': 'Bearer ' + token
                        },
                        data: data
                    }
                    const result = axios(options).catch(function (error) {
                        if (error.response.status == 401) {
                            window.location.replace(url + "/admin/logout");
                        }
                        throw error.response;
                    });
                    resolve(result);
                },
                No: function () {

                }
            }
        });
    })
}

async function requestAPI (method, apiUrl, data, isFormData = null) {
    const token = await getToken();

    let contentType = "application/json"
    if (isFormData)
        contentType = "multipart/form-data";

    const options = {
        method: method,
        url: url + "/" + apiUrl,
        headers: {
            'content-Type': contentType,
            'authorization': 'Bearer ' + token
        },
        data: data
    }
    const result = await axios(options).catch(function (error) {
        if (error.response.status == 401) {
            window.location.replace(url + "/admin/logout");
        }

        throw error.response;
    });
    return result.data;
}

function clearValuesByIds (arrayId) {
    $.each(arrayId, function (k, v) {
        $("#" + v).val("");
    })
}

function validationNullByIds (arrayId) {
    var count = false;
    $.each(arrayId, function (k, v) {
        var data = $('#' + v).val();
        if (data == "" || data == null) {
            $('#frm-group-' + v).addClass("has-error");
            $('#frm-group-' + v + " span.help-block").text('input required');
            count = true;
        } else {
            $('#frm-group-' + v).removeClass("has-error");
            $('#frm-group-' + v + " span.help-block").text('');
        }
    })
    if (count == true) {
        return true;
    }
}

//readURL_img(this,2048,2048,2048000,'blah','pic');
function readURL_img (input, heights, widths, sizes, id_target, pic) {
    var a = input.files[0];

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result;
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height > heights || width > widths || a.size > sizes) {
                    $.alert({
                        title: 'INPUT ERROR',
                        content: "Please Input Image size less than " + (sizes / 1024000) + "M,width less than " + widths + "px && height less than " + heights + "px !!!",
                    });
                    $(input).val('');
                } else {
                    $('#' + id_target + '').attr('src', pic);
                }
            };
        }
        reader.readAsDataURL(input.files[0]);
    }
}
//readURL(this,2048,2048,2048000,'blah');
function readURL (input, heights, widths, sizes, id_target) {
    var a = input.files[0];

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            var image = new Image();
            image.src = e.target.result;
            image.onload = function () {
                var height = this.height;
                var width = this.width;
                if (height > heights || width > widths || a.size > sizes) {
                    $.alert({
                        title: 'INPUT ERROR',
                        content: "Please Input Image size less than " + (sizes / 1024000) + "M,width less than " + widths + "px && height less than " + heights + "px !!!",
                    });

                    $(input).val('');
                } else {
                    $('#' + id_target + '').attr('src', e.target.result);
                }


            };

        }
        reader.readAsDataURL(input.files[0]);
    } else {
        $('#' + id_target + '').attr('src', url + "/resources/back-end/img/no_image.jpg");
    }
}

function getPagination (result) {
    var htmlPage = "";
    const totalPage = result.totalPages;
    if (totalPage > 0) {
        var pageNumberHtml = "";
        var backHtml = "";
        var nextHtml = "";

        for (var i = 1; i <= totalPage; i++) {
            var currentPage = result.pageable.pageNumber;
            if (currentPage == (i - 1))
                pageNumberHtml += '<li class="active"><a href="javascript:void(0)">' + i + '</a></li>';
            else
                pageNumberHtml += '<li onclick="gets(' + (i - 1) + ')"><a href="javascript:void(0)">' + i + '</a></li>';
        }
        if (!result.last)
            nextHtml = '<li onclick="gets(' + (currentPage + 1) + ')"><a href="javascript:void(0)">&raquo;</a></li>';
        else
            nextHtml = '<li><a href="javascript:void(0)">&raquo;</a></li>';

        if (!result.first)
            backHtml = '<li onclick="gets(' + (currentPage - 1) + ')"><a href="javascript:void(0)">&laquo;</a></li>';
        else
            backHtml = '<li><a href="javascript:void(0)">&laquo;</a></li>';

        htmlPage = backHtml + pageNumberHtml + nextHtml;
    } else {
        htmlPage = '<li><a href="javascript:void(0)">&laquo;</a></li><li><a href="javascript:void(0)">&raquo;</a></li>';
    }
    $('.pagination').html(htmlPage);
}

function preview (docName) {
    if (docName == null || docName === "")
        return false;
    const extName = docName.substr((docName.lastIndexOf('.') + 1)).toLowerCase();
    const fileUrl = docName;
    if (extName === 'pdf') {
        PDFObject.embed(fileUrl, "#doc-dev");
        $('#modal-preview-doc').modal('toggle');
        $('#btn-download-doc').attr('href', fileUrl);
        $('#btn-link-doc').attr('href', fileUrl);
    } else {
        $('#btn-link-img').attr('href', fileUrl);
        $('#btn-download-img').attr('href', fileUrl);
        $('#img-doc-dev').html('<img src="' + fileUrl + '" class="img-rounded preview-img-size" alt="doc" >');
        $('#modal-preview-doc-img').modal('toggle');
    }
}

$('.allow-decimal').keypress(function (event) {

    var $this = $(this);
    if ((event.which != 46 || $this.val().indexOf('.') != -1) &&
        ((event.which < 48 || event.which > 57) && (event.which != 0 && event.which != 8))) {
        event.preventDefault();
    }
    var text = $(this).val();
    if ((event.which == 46) && (text.indexOf('.') == -1)) {
        setTimeout(function () {
            if ($this.val().substring($this.val().indexOf('.')).length > 2) {
                $this.val($this.val().substring(0, $this.val().indexOf('.') + 2));
            }
        }, 1);
    }
    if ((text.indexOf('.') != -1) && (text.substring(text.indexOf('.')).length > 2) && (event.which != 0 && event.which != 8) &&
        ($(this)[0].selectionStart >= text.length - 2)) {
        event.preventDefault();
    }
});

//<input  class="form-control allow-decimal">

$('.allow-decimal').bind("paste", function (e) {
    var text = e.originalEvent.clipboardData.getData('Text');
    if ($.isNumeric(text)) {
        if ((text.substring(text.indexOf('.')).length > 4) && (text.indexOf('.') > -1)) {
            e.preventDefault();
            $(this).val(text.substring(0, text.indexOf('.') + 2));
        }
    } else {
        e.preventDefault();
    }

});

function permissionCheck (codeName, attrId) {
//    const permissions = JSON.parse(localStorage.getItem("permissions"));
//    const auth = permissions.includes("ALL_FUNCTIONS") || permissions.includes(codeName);
//    if (!auth)
//        $('.' + attrId).addClass('hidden');
//    else
//        $('.' + attrId).removeClass('hidden');
}

function permissionRead (codeName, attrId) {
//    const permissions = JSON.parse(localStorage.getItem("permissions"));
//    const auth = permissions.includes("ALL_FUNCTIONS") || permissions.includes("ALL_FUNCTIONS_READ") || permissions.includes(codeName);
//    if (!auth)
//        $('.' + attrId).addClass('hidden');
//    else
//        $('.' + attrId).removeClass('hidden');
}

function exportExcel (tableId, fileName) {
    $('#' + tableId).tableExport({ type: 'excel', fileName: fileName + " " + moment(new Date()).format("DDMMYYYY") });
}

function exportPdf (tableId, fileName) {
    $('#' + tableId).tableExport({
        type: 'pdf',
        fileName: fileName + " " + moment(new Date()).format("DDMMYYYY"),
        jspdf: {
            orientation: 'l',
            format: 'a3',
            margins: { left: 10, right: 10, top: 20, bottom: 20 },
            autotable: {
                styles: {
                    cellPadding: 2,
                    rowHeight: 12,
                    fontSize: 8,
                    fillColor: 255,
                    textColor: 50,
                    overflow: 'ellipsize',
                    halign: 'inherit',
                    valign: 'middle'
                },
                tableWidth: 'auto'
            }
        }
    });
}

$.fn.serializeFormJSON = function () {

    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name.trim()]) {
            if (!o[this.name.trim()].push) {
                o[this.name.trim()] = [o[this.name.trim()]];
            }
            o[this.name.trim()].push(this.value.trim() || '');
        } else {
            o[this.name.trim()] = this.value.trim() || '';
        }
    });
    return o;
};