$(async function () {
    $('#main-body').addClass('sidebar-collapse');
    await gets();

});

async function gets (page = null, data = null) {
    Pace.restart();
    const limit = $('#show-qty').val();

    page == null ? 0 : page;
    var result = await getAllData("api/member/all?page=" + page + "&size=" + limit, data);
    getPagination(result, this);
    var html = "";
    var no = result.number * result.size;
    $.each(result.content, function (k, v) {
        status = '';
        if (v.status === 'ACTIVE')
            status = '<span class="label label-success">Active</span>';
        else if (v.status === 'PENDING')
            status = '<span class="label label-warning">Pending</span>';
        else if (v.status === 'APPROVED')
            status = '<span class="label label-primary">Approved</span>';
        else
            status = '<span class="label label-danger">Rejected</span>';

        var reject = '<li class="btn-reject-loan"><a href="javascript:void(0)" onclick="showReject(' + v.id + ')">Reject</a></li>';
        var approve = '<li class="btn-approve-loan" onclick="approve(' + v.id + ')" ><a href="javascript:void(0)">Approve</a></li>';

        html += '<tr>'
            + '<td>' + (k + no + 1) + '</td>'
            + '<td>' + v.khmerFullName + '</td>'
            + '<td>' + (v.englishFullName == null ? '' : v.englishFullName) + '</td>'
            + '<td>' + v.sex + '</td>'
            + '<td>' + v.dateOfBirth + '</td>'
            + '<td>' + v.idCardNo + '</td>'
            + '<td>' + '<a onclick="preview(\'' + v.idCardImageFront + '\')"><i class="fa fa-fw fa fa-eye"></i></a>' + '</td>'
            + '<td>' + '<a onclick="preview(\'' + v.idCardImageBack + '\')"><i class="fa fa-fw fa fa-eye"></i></a>' + '</td>'
            + '<td>' + v.type + '</td>'
            + '<td>' + (v.address.addressLIne == null ? '' : v.address.addressLIne) + '</td>'
            + '<td>' + v.address.village + '</td>'
            + '<td>' + v.address.commune + '</td>'
            + '<td>' + v.address.district + '</td>'
            + '<td>' + v.address.province + '</td>'
            + '<td>' + status + '</td>'
            + '<td>' + (v.rejectReason == null ? '' : v.rejectReason) + '</td>'
            + '<td class="text-left" style="width: 8%">'
            + '<div class="btn-group">' +
                    '    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false">' +
                    '        <span class="caret"></span>' +
                    '        <span class="sr-only">Toggle Dropdown</span>' +
                    '    </button>' +
                    '    <ul class="dropdown-menu" role="menu">' +
                    approve +
                    reject +
                    '    </ul>' +
                    '</div>';
            + '</td>'
            + '</tr>';
    })
    $('#table-data tbody').html(html);
}

async function approve (id) {
    try {
        var data = {
            "command": "APPROVE"
        }
        await submitData('POST', 'api/member/' + id, data);
        await gets();
    } catch (error) {
        console.log(error);
    }
}

function showReject(id) {
    $('#id').val(id);
    $("#modal-reject-member").modal('toggle')
    $("#description").focus();
}

$('#form-reject-member').submit(async function (e) {
    e.preventDefault();

    const id = $('#id').val();
    const reason = $('#reason').val();

     const formData = new FormData();
     formData.append('command', "REJECT");
     formData.append('reason', reason);

    try {
        await submitData('POST', 'api/member/' + id, formData, true);
        await gets();
        $("#modal-reject-member").modal('toggle');
    } catch (error) {
        console.log(error);
        $('#member-reject-error-message').text(error.data.errors[0]);
    }
})

$('#frm-search').on('submit', async function (e) {
    e.preventDefault();
    const data = $(this).serializeFormJSON();
    await gets(0, data);
})

$('#export-pdf').on('click', function () {
    exportPdf('table-data', 'List Member');
})

$('#export-excel').on('click', function () {
    exportExcel('table-data', 'List Member');
})

$('#show-qty').on('change', async function () {
    const data = $('#frm-search').serializeFormJSON();
    await gets(0, data);
})