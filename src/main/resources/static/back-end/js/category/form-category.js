$(function () {
    getById();
});

$('#btn-list').on('click', function () {
    window.location.assign(url + "/admin/category");
})

$('#btn-clear').on('click', function () {
    clearValuesByIds(["name", "priority"]);
})

async function getById () {
    var id = $('#id').val();
    if (id != null) {

        var result = await getAllData("api/category/" + id);
        $('#name').val(result.name);
        $('#priority').val(result.priority);
    }
}

$('#frm-submit').submit(async function (e) {
    e.preventDefault();

    const id = $('#id').val();

    const name = $('#name').val();
    const priority = $('#priority').val();
    const data = {
        name: name,
        priority: priority
    }
    if (id != null && id != "") {
        try {
            await submitData('PUT', 'api/category/' + id, data);
            $('#btn-list').click();
        } catch (error) {
            $('#error-message').text(error.data.errors[0]);
        }
    } else {
        try {
            await submitData('POST', 'api/category', data);
            $('#btn-list').click();
        } catch (error) {
            throw error;
            $('#error-message').text(error.data.errors[0]);
        }
    }
})