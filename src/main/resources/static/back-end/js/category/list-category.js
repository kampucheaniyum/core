$(function () {
    gets();
});

async function gets () {
    var result = await getAllData("api/category");
    var html = "";
    $.each(result, function (k, v) {
        html += '<tr>'
            + '<td>' + (k + 1) + '</td>'
            + '<td>' + v.priority + '</td>'
            + '<td>' + v.name + '</td>'
                + '<td class="text-center">'
                        + '<button type="button" onclick="plus(' + v.id + ')" class="btn btn-xs btn-success btn-update-news"><i class="fa fa-fw fa-arrow-up"></i></button>&nbsp;&nbsp;'
                        + '<button type="button" onclick="minus(' + v.id + ')" class="btn btn-xs btn-primary btn-delete-news"><i class="fa fa-fw fa fa-arrow-down"></i></button>'
                + '</td>'
            + '<td class="text-center">'
            + '<button type="button" onclick="editAction(' + v.id + ')" class="btn btn-xs btn-warning btn-update-category"><i class="fa fa-fw fa-pencil-square-o"></i></button>&nbsp;&nbsp;'
            + '<button type="button" onclick="deleteAction(' + v.id + ')" class="btn btn-xs btn-danger btn-delete-user"><i class="fa fa-fw fa fa-trash-o"></i></button>'
            + '</td>'
            + '</tr>';
    })
    $('#table-data tbody').html(html);
}

async function plus (id) {
    const data = {};
    await requestAPI ('POST', 'api/category/' + id + '/priority?command=increase', data, false)
    await gets();
}

async function minus (id) {
    const data = {};
    await requestAPI ('POST', 'api/category/' + id + '/priority?command=decrease', data, false)
    await gets();
}

function editAction (id) {
    window.location.assign(url + "/admin/category/" + id);
}

$('#export-pdf').on('click', function () {
    Pace.restart();
    exportPdf('table-data', 'List Category');
})

async function deleteAction (id) {
    try {
        const del = await deleteData("api/category/" + id);
        if (del)
            await gets();
    } catch (error) {
        console.log(error);
    }
}

$('#export-excel').on('click', function () {
    Pace.restart();
    exportExcel('table-data', 'List Category');
})
