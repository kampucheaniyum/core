package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminCategoryController {

    private final static String MENU_NAME = "Category";

    @RequestMapping("/category")
    public String getCategory(final Model model) {
        model.addAttribute("menu", MENU_NAME);
        model.addAttribute("subMenu", "List " + MENU_NAME);
        return "back-end/administration/category/list-category";
    }

    @RequestMapping("/category/add")
    public String createCategory(final Model model) {
        model.addAttribute("menu", MENU_NAME);
        model.addAttribute("subMenu", "Create " + MENU_NAME);
        return "back-end/administration/category/form-category";
    }

    @RequestMapping("/category/{id}")
    public String updateCategory(@PathVariable final Long id,
                                 final Model model) {
        model.addAttribute("id", id);
        model.addAttribute("menu", MENU_NAME);
        model.addAttribute("subMenu", "Update " + MENU_NAME);
        return "back-end/administration/category/form-category";
    }
}
