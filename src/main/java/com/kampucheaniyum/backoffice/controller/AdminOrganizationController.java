package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminOrganizationController {

	@GetMapping("/about-us")
	public String aboutUs(final Model model) {
		model.addAttribute("menu", "About Us");
		model.addAttribute("subMenu", "List About Us");
		return "back-end/administration/about-us/form-about-us";
	}

	@GetMapping("/history")
	public String history(final Model model) {
		model.addAttribute("menu", "History");
		model.addAttribute("subMenu", "List History");
		return "back-end/administration/history/form-history";
	}
}
