package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminContentController {

	@RequestMapping("/content")
	public String getNewsAndArticle(final Model model) {
		model.addAttribute("menu", "Content");
		model.addAttribute("subMenu", "List Content");
		return "back-end/administration/content/list-content";
	}

	@RequestMapping("/content/add")
	public String editNewsAndArticle(final Model model) {
		model.addAttribute("menu", "Content");
		model.addAttribute("subMenu", "Create Content");
		return "back-end/administration/content/form-content";
	}

	@RequestMapping("/content/{id}")
	public String editNewsAndArticle(final Model model,
									 @PathVariable final Long id) {
		model.addAttribute("id", id);
		model.addAttribute("menu", "Content");
		model.addAttribute("subMenu", "Update Content");
		return "back-end/administration/content/form-content";
	}
}
