package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller
@RequestMapping("/admin")
public class UserController {

	private final static String MENU_NAME = "User";

	@RequestMapping("/user")
	public String index(final Model model) {
		model.addAttribute("menu", MENU_NAME);
		model.addAttribute("subMenu", "List User");
		return "back-end/administration/user/list-user";
	}

	@RequestMapping("/user/add")
	public String add(final Model model) {
		model.addAttribute("menu", MENU_NAME);
		model.addAttribute("subMenu", "Create User");
		return "back-end/administration/user/form-user";
	}

	@RequestMapping("/user/{id}")
	public String update(@NotNull @PathVariable("id") final Long id, final Model model) {
		model.addAttribute("id", id);
		model.addAttribute("menu", MENU_NAME);
		model.addAttribute("subMenu", "Update User");
		return "back-end/administration/user/form-user";
	}
}
