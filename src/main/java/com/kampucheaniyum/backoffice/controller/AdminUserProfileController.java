package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.constraints.NotNull;

@Controller
public class AdminUserProfileController {

    private final static String MENU_NAME = "User";

    @RequestMapping("/user/{id}/profile")
    public String viewUserDetail(@NotNull @PathVariable("id") final Long id,
                                 final Model model) {
        model.addAttribute("id", id);
        model.addAttribute("menu", MENU_NAME);
        model.addAttribute("subMenu", "View User Profile");
        return "back-end/administration/user/form-user-detail";
    }
}