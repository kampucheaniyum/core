package com.kampucheaniyum.backoffice.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminMemberController {

	@GetMapping("/member")
	public String member(final Model model) {
		model.addAttribute("menu", "Member");
		model.addAttribute("subMenu", "List Member");
		return "back-end/administration/member/list-member";
	}
}
