package com.kampucheaniyum;

import lombok.extern.slf4j.Slf4j;
import org.springframework.core.env.MutablePropertySources;
import org.springframework.core.env.StandardEnvironment;
import org.springframework.core.io.support.ResourcePropertySource;

import java.net.URL;

@Slf4j
public class Environment {

    private static final String PROPERTY_FILE = "application.properties";

    private static final StandardEnvironment SPRING_ENVIRONMENT = new StandardEnvironment() {
        @Override
        protected void customizePropertySources(final MutablePropertySources propertySources) {
            try {
                final URL resource = Environment.class.getClassLoader().getResource(PROPERTY_FILE);
                if (resource != null) {
                    propertySources.addLast(new ResourcePropertySource(resource.toString()));
                }
                super.customizePropertySources(propertySources);
            } catch (final Exception e) {
                log.warn(e.getMessage());
            }
        }
    };

    public static String getServerHost() {
        return SPRING_ENVIRONMENT.getProperty("server.host");
    }
}
