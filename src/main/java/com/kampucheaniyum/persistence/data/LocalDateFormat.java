package com.kampucheaniyum.persistence.data;

public class LocalDateFormat {
    public static final String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    public static final String CARD_NUMBER_FORMAT = "yyyyMMddHHmmss";
}
