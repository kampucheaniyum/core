package com.kampucheaniyum.persistence.data;

import lombok.Data;

import java.util.List;

@Data
public class IdList {
    private List<Long> ids;
}
