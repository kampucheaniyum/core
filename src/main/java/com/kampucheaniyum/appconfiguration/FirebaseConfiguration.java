package com.kampucheaniyum.appconfiguration;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.messaging.FirebaseMessaging;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.FileInputStream;
import java.io.IOException;

@Configuration
public class FirebaseConfiguration {

    @Bean
    public FirebaseMessaging firebaseMessaging() {
        final String path = "src/main/resources/firebase/serviceAccountKey.json";
        final String accountId = "firebase-adminsdk-24ars@party-93ba1.iam.gserviceaccount.com";
        FirebaseApp firebaseApp;
        try {
            var serviceAccount = new FileInputStream(path);
            var options = FirebaseOptions.builder()
                    .setCredentials(GoogleCredentials.fromStream(serviceAccount))
                    .setServiceAccountId(accountId)
                    .build();
            firebaseApp = FirebaseApp.initializeApp(options);
        } catch (IOException e) {
            throw new RuntimeException("Fail to initialize firebase admin");
        }
        return FirebaseMessaging.getInstance(firebaseApp);
    }
}
