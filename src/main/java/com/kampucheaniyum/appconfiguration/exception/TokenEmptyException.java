package com.kampucheaniyum.appconfiguration.exception;

public class TokenEmptyException extends RuntimeException{
    public TokenEmptyException() {
        super("Access token is empty");
    }
}
