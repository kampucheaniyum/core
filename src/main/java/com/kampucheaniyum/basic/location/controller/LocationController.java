package com.kampucheaniyum.basic.location.controller;

import com.kampucheaniyum.basic.location.domain.Location;
import com.kampucheaniyum.basic.location.domain.LocationType;
import com.kampucheaniyum.basic.location.repository.LocationRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("api/location")
@RequiredArgsConstructor
public class LocationController {

    private final LocationRepository repository;

    @GetMapping("province")
    public List<Location> getProvince() {
        return repository.findAllByParentId(1L);
    }

    @GetMapping("district")
    public List<Location> getDistrict(@RequestParam("parentId") Long id) {
        return repository.findAllByLocationTypeAndParentId(LocationType.DISTRICT, id);
    }

    @GetMapping("commune")
    public List<Location> getCommune(@RequestParam("parentId") Long id) {
        return repository.findAllByLocationTypeAndParentId(LocationType.COMMUNE, id);
    }

    @GetMapping("village")
    public List<Location> getVillage(@RequestParam("parentId") Long id) {
        return repository.findAllByLocationTypeAndParentId(LocationType.VILLAGE, id);
    }
}
