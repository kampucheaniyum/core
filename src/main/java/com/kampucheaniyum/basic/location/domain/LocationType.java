package com.kampucheaniyum.basic.location.domain;

public enum LocationType {
    COUNTRY,
    PROVINCE,
    DISTRICT,
    COMMUNE,
    VILLAGE
}
