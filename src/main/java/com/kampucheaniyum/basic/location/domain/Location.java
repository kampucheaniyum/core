package com.kampucheaniyum.basic.location.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.kampucheaniyum.persistence.domain.VersionEntity;
import lombok.Getter;
import org.springframework.data.annotation.Immutable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Getter
@Immutable
@Table(name = "location")
public class Location extends VersionEntity {

    @Column(name = "name_kh")
    private String nameKh;

    @Column(name = "name_en")
    private String nameEn;

    @JsonIgnore
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "parent_id")
    private Location parent;

    @Enumerated(EnumType.STRING)
    private LocationType locationType;

    @Column(name = "location_code")
    private String locationCode;
}
