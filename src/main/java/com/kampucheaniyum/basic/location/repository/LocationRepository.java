package com.kampucheaniyum.basic.location.repository;

import com.kampucheaniyum.basic.location.domain.Location;
import com.kampucheaniyum.basic.location.domain.LocationType;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface LocationRepository extends JpaRepository<Location, Long> {
    List<Location> findAllByParentId(Long id);

    List<Location> findAllByLocationTypeAndParentId(LocationType locationType, Long id);
}