package com.kampucheaniyum.basic.notification.listener;

import com.kampucheaniyum.appconfiguration.utils.AutowiringHelper;
import com.kampucheaniyum.basic.notification.domain.AppNotification;
import com.kampucheaniyum.basic.notification.service.FirebaseService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.support.TransactionSynchronization;
import org.springframework.transaction.support.TransactionSynchronizationManager;

import javax.persistence.PostPersist;

public class NotificationListener {

    @PostPersist
    public void postPersist(AppNotification appNotification) {
        TransactionSynchronizationManager.registerSynchronization(new TransactionAdapter(appNotification));
    }

    @RequiredArgsConstructor
    private static class TransactionAdapter implements TransactionSynchronization {

        private final AppNotification appNotification;

        @Autowired
        private FirebaseService messagingService;

        @Override
        public void afterCommit() {
            AutowiringHelper.autowire(this, messagingService);
            messagingService.sendNotification(appNotification);
        }
    }
}
