package com.kampucheaniyum.basic.notification.repository;

import com.kampucheaniyum.basic.notification.domain.AppNotification;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface NotificationRepository extends JpaRepository<AppNotification, Long> {

    @Query("SELECT an from AppNotification an WHERE an.appUser = :user OR an.appUser IS NULL")
    Page<AppNotification> getByUserIdOrNull(@Param("user") AppUser user, Pageable pageable);
}
