package com.kampucheaniyum.basic.notification.domain;

public enum NotificationStatus {
    NEW,
    SEEN,
    DELETED
}
