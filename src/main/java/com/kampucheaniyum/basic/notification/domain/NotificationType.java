package com.kampucheaniyum.basic.notification.domain;

public enum NotificationType {
    MEMBER_REQUEST_APPROVED,
    NEW_CONTENT_RELEASED,
    BROADCAST
}
