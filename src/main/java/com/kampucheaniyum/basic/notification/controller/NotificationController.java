package com.kampucheaniyum.basic.notification.controller;

import com.kampucheaniyum.appconfiguration.utils.ApplicationSecurityContext;
import com.kampucheaniyum.basic.notification.domain.AppNotification;
import com.kampucheaniyum.basic.notification.repository.NotificationRepository;
import com.kampucheaniyum.exception.UnAuthorizeException;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("api/notification")
@RequiredArgsConstructor
public class NotificationController {

    private final NotificationRepository repository;
    private final ApplicationSecurityContext context;

    @GetMapping
    @PreAuthorize("hasAnyAuthority('USER')")
    public Page<AppNotification> getUserNotification(Pageable pageable) {
        var user = context.authenticatedUser();
        if (user == null) {
            throw new UnAuthorizeException();
        }
        return repository.getByUserIdOrNull(user, pageable);
    }
}
