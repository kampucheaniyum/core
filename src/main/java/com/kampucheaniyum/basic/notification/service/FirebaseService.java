package com.kampucheaniyum.basic.notification.service;

import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.MulticastMessage;
import com.google.firebase.messaging.Notification;
import com.kampucheaniyum.basic.notification.domain.AppNotification;
import com.kampucheaniyum.user.authentication.domain.FireBaseToken;
import com.kampucheaniyum.user.authentication.repository.FireBaseTokenRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class FirebaseService {

    private final FirebaseMessaging firebaseMessaging;
    private final FireBaseTokenRepository tokenRepository;

    @Async
    public void sendNotification(AppNotification appNotification) {
        var notification = Notification
                .builder()
                .setTitle(appNotification.getTitle())
                .setBody(appNotification.getDetail())
                .build();

        var data = new HashMap<String, String>();
        data.put("type", appNotification.getType().name());
        data.putAll(appNotification.getData());

        MulticastMessage message;
        if (appNotification.getAppUser() != null) {
            var tokens = tokenRepository.findAllByCreatedBy(appNotification.getAppUser())
                    .stream()
                    .map(FireBaseToken::getToken)
                    .collect(Collectors.toList());
            if (tokens.isEmpty()) return;
            message = MulticastMessage.builder()
                    .setNotification(notification)
                    .addAllTokens(tokens)
                    .putAllData(data)
                    .build();
        } else {
            message = MulticastMessage.builder()
                    .setNotification(notification)
                    .putAllData(data)
                    .build();
        }

        try {
            var response = firebaseMessaging.sendMulticast(message);
            log.info("Successfully sent {} notifications", response.getSuccessCount());
        } catch (Exception e) {
            log.error("Fail to send message to firebase", e);
            throw new RuntimeException(e);
        }
    }
}
