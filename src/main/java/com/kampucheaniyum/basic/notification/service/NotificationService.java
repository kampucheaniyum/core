package com.kampucheaniyum.basic.notification.service;

import com.kampucheaniyum.basic.notification.domain.AppNotification;
import com.kampucheaniyum.basic.notification.domain.NotificationType;
import com.kampucheaniyum.basic.notification.repository.NotificationRepository;
import com.kampucheaniyum.portfolio.content.domain.Content;
import com.kampucheaniyum.user.member.domain.Member;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.Map;

@Slf4j
@Service
@Async
@RequiredArgsConstructor
@Transactional(propagation = Propagation.REQUIRES_NEW)
public class NotificationService {

    private final NotificationRepository repository;

    public void memberApproved(Member member) {
        var notification = new AppNotification()
                .setTitle("Your member request has been approved")
                .setDetail("Your member request has been approved")
                .setAppUser(member.getCreatedBy())
                .setImage(member.getCreatedBy().getPhotoUrl())
                .setData(Map.of("id", "" + member.getCreatedBy().getId()))
                .setType(NotificationType.MEMBER_REQUEST_APPROVED);
        repository.save(notification);
    }

    public void contentPublished(Content content) {
        var notification = new AppNotification()
                .setTitle(content.getTitle())
                .setDetail(content.getContent())
                .setData(Map.of("id", "" + content.getId()))
                .setType(NotificationType.NEW_CONTENT_RELEASED);
        repository.save(notification);
    }

    public void sendBroadcast(String title, String content) {
        var notification = new AppNotification()
                .setTitle(title)
                .setDetail(content)
                .setType(NotificationType.BROADCAST);
        repository.save(notification);
    }
}
