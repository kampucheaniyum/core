package com.kampucheaniyum.basic.health.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("api/health")
public class HealthController {

    @GetMapping
    public String sayOk(HttpServletRequest request) {
        log.info("request host is:{}", request.getRequestURL());
        return "ok";
    }

    @GetMapping("/isloggedin")
    public boolean isLoggedIn() {
        return true;
    }
}
