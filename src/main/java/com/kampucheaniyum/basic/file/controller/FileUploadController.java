package com.kampucheaniyum.basic.file.controller;

import com.kampucheaniyum.basic.file.data.FileData;
import com.kampucheaniyum.basic.file.data.FileType;
import com.kampucheaniyum.basic.file.storage.StorageService;
import com.kampucheaniyum.basic.file.utils.FileNameUtil;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import javax.validation.constraints.NotNull;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static com.kampucheaniyum.basic.file.data.FileType.OTHER;


@Slf4j
@RestController
@RequiredArgsConstructor
public class FileUploadController {

    private final StorageService storageService;

    @GetMapping("/files/{filename:.+}")
    @ResponseBody
    public byte[] serveFile(@PathVariable String filename) throws IOException {
        final var file = storageService.loadAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @DeleteMapping(value = "/files/{filename:.+}")
    @Transactional
    public void deleteFile(@PathVariable String filename) throws IOException {
        storageService.deleteByName(filename);
    }

    @GetMapping(
            value = "/small/image/{filename:.+}", produces = {
            MediaType.IMAGE_JPEG_VALUE,
            MediaType.IMAGE_GIF_VALUE,
            MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] serveSmallImage(@PathVariable String filename) throws IOException {
        final var file = storageService.loadSmallAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @GetMapping(
            value = "/medium/image/{filename:.+}",
            produces = {
                    MediaType.IMAGE_JPEG_VALUE,
                    MediaType.IMAGE_GIF_VALUE,
                    MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] serveMediumImage(@PathVariable String filename) throws IOException {
        final var file = storageService.loadMediumAsResource(filename);
        return file.getInputStream().readAllBytes();
    }

    @GetMapping(
            value = {"/image/{filename:.+}", "/image/{filename:.+}/{width}/{height}"},
            produces = {
                    MediaType.IMAGE_JPEG_VALUE,
                    MediaType.IMAGE_GIF_VALUE,
                    MediaType.IMAGE_PNG_VALUE})
    @ResponseBody
    public byte[] serveImage(@PathVariable("filename") @NotNull final String filename,
                             @RequestParam(value = "width", required = false) final Integer qWidth,
                             @RequestParam(value = "height", required = false) final Integer qHeight,
                             @PathVariable(value = "width", required = false) final Integer pWidth,
                             @PathVariable(value = "height", required = false) final Integer pHeight) throws IOException {
        final var file = storageService.loadAsResource(filename);
        final var width = qWidth == null ? pWidth : qWidth;
        final var height = qHeight == null ? pHeight : qHeight;
        if (width == null || height == null) {
            return file.getInputStream().readAllBytes();
        }
        final var thumbOutput = new ByteArrayOutputStream();
        thumbOutput.reset();
        Thumbnails.of(file.getInputStream())
                .size(width, height)
                .useOriginalFormat()
                .toOutputStream(thumbOutput);
        return thumbOutput.toByteArray();
    }

    @PostMapping("/api/upload")
    public FileData handleFileUpload(HttpServletRequest request, @RequestParam("file") MultipartFile file) {
        if (file == null || file.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file is empty");
        }
        var fileName = file.getOriginalFilename();
        if (fileName == null || fileName.isBlank()) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file name not found");
        }
        if (OTHER.equals(FileNameUtil.getFileType(fileName))) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "file type does not support");
        }
        final var name = storageService.store(file);
        final var fileType = FileNameUtil.getFileType(name);
        final var uri = FileType.IMAGE.equals(fileType) ? String.format("/image/%s", name) : String.format("/file/%s", name);
        final var url = request.getRequestURL()
                .toString()
                .replace("/api/upload", uri);
        final var newFile = new FileData()
                .setFileName(name)
                .setFileType(fileType)
                .setUri(uri)
                .setUrl(url);
        if (newFile.getFileType() == FileType.IMAGE) {
            storageService.writeSmallOutput(name);
            storageService.writeMediumOutput(name);
        }
        return newFile;
    }

}
