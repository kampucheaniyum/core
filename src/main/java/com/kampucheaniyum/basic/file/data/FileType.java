package com.kampucheaniyum.basic.file.data;

public enum FileType {
    IMAGE,
    VIDEO,
    SOUND,
    OTHER,
    DOCUMENT;
}
