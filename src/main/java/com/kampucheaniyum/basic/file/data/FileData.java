package com.kampucheaniyum.basic.file.data;

import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class FileData {

    private String fileName;

    private String url;

    private String uri;

    private FileType fileType;
}
