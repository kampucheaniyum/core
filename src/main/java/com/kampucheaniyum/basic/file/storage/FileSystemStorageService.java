package com.kampucheaniyum.basic.file.storage;

import lombok.extern.slf4j.Slf4j;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpStatus;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.server.ResponseStatusException;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.PosixFilePermissions;
import java.util.Optional;
import java.util.UUID;
import java.util.stream.Stream;

@Slf4j
@Service
public class FileSystemStorageService implements StorageService {

    private final Path rootLocation;
    private final Path smallLocation;
    private final Path mediumLocation;
    private final StorageProperties properties;

    public FileSystemStorageService(final StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
        this.smallLocation = Paths.get(properties.getSmallLocation());
        this.mediumLocation = Paths.get(properties.getMediumLocation());
        this.properties = properties;
    }

    @PostConstruct
    private void postConstruct() throws IOException {
        var isLinux = Optional.ofNullable(System.getProperty("os.name"))
                .filter(name -> name.contains("nux"))
                .isPresent();
        if (!rootLocation.toFile().exists()) {
            Files.createDirectory(rootLocation);
            if (isLinux) {
                Files.setPosixFilePermissions(rootLocation, PosixFilePermissions.fromString("rwxrwxrwx"));
            }
        }
        if (!smallLocation.toFile().exists()) {
            Files.createDirectory(smallLocation);
            if (isLinux) {
                Files.setPosixFilePermissions(smallLocation, PosixFilePermissions.fromString("rwxrwxrwx"));
            }
        }
        if (!mediumLocation.toFile().exists()) {
            Files.createDirectory(mediumLocation);
            if (isLinux) {
                Files.setPosixFilePermissions(mediumLocation, PosixFilePermissions.fromString("rwxrwxrwx"));
            }
        }
    }

    @Override
    public String store(@Valid @NotNull MultipartFile file) {
        if (file.getOriginalFilename() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "File name not found");
        }
        String filename = StringUtils.cleanPath(file.getOriginalFilename());
        try {
            if (file.isEmpty()) {
                throw new StorageException("Failed to store empty file " + filename);
            }
            if (filename.contains("..")) {
                // This is a security check
                throw new StorageException(
                        "Cannot store file with relative path outside current directory "
                                + filename);
            }
            try (InputStream inputStream = file.getInputStream()) {
                var name = newName(filename);
                Files.copy(inputStream, this.rootLocation.resolve(name),
                        StandardCopyOption.REPLACE_EXISTING);
                return name;
            }
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + filename, e);
        }
    }

    @Async
    @Override
    public void writeSmallOutput(final String name) {
        try {
            Thumbnails.of(load(name).toFile())
                    .size(properties.getSmallWidth(), properties.getSmallHeight())
                    .toFile(smallLocation.resolve(name).toFile());
        } catch (Exception e) {
            log.error("error convert image with file name: " + name, e);
            try {
                Files.copy(load(name), loadSmall(name));
            } catch (Exception exception) {
                log.error("error copy file", e);
            }
        }
    }

    @Async
    @Override
    public void writeMediumOutput(final String name) {
        try {
            Thumbnails.of(load(name).toFile())
                    .size(properties.getMediumWidth(), properties.getMediumHeight())
                    .useOriginalFormat()
                    .toFile(mediumLocation.resolve(name).toFile());
        } catch (Exception e) {
            log.error("error convert image with file name: " + name, e);
            try {
                Files.copy(load(name), loadMedium(name));
            } catch (Exception exception) {
                log.error("error copy file", e);
            }
        }
    }

    private String newName(@NotNull final String name) {
        return UUID.randomUUID().toString().replaceAll("-", "")
                + "." + StringUtils.getFilenameExtension(name);
    }

    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                    .filter(path -> !path.equals(this.rootLocation))
                    .map(this.rootLocation::relativize);
        } catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }
    }

    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    private Path loadSmall(String filename) {
        return smallLocation.resolve(filename);
    }

    private Path loadMedium(String filename) {
        return mediumLocation.resolve(filename);
    }

    @Override
    public Resource loadAsResource(String filename) {
        final var file = load(filename);
        return loadResource(file, filename);
    }

    @Override
    public Resource loadSmallAsResource(final String filename) {
        final var file = loadSmall(filename);
        return loadResource(file, filename);
    }

    @Override
    public Resource loadMediumAsResource(final String filename) {
        final var file = loadMedium(filename);
        return loadResource(file, filename);
    }

    private Resource loadResource(final Path file, final String filename) {
        try {
            final var resource = new UrlResource(file.toUri());
            if (resource.exists() || resource.isReadable()) {
                return resource;
            } else {
                throw new StorageFileNotFoundException("Could not read file: " + filename);
            }
        } catch (MalformedURLException e) {
            throw new StorageFileNotFoundException("Could not read file: " + filename, e);
        }
    }

    @Override
    public boolean deleteByName(final String filename) {
        try {
            final var file = load(filename).toFile();
            final var xFile = loadSmall(filename).toFile();
            final var xxFile = loadMedium(filename).toFile();
            final var zin = file.delete();
            final var x = xFile.delete();
            final var xx = xxFile.delete();
            return zin && x && xx;
        } catch (final Exception e) {
            log.error("error delete file", e);
            return false;
        }
    }

    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        } catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }
}
