package com.kampucheaniyum.basic.file.storage;

import org.springframework.core.io.Resource;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Path;
import java.util.stream.Stream;

public interface StorageService {

    void init();

    void writeSmallOutput(final String filename);

    void writeMediumOutput(final String filename);

    String store(MultipartFile file);

    Stream<Path> loadAll();

    Path load(String filename);

    Resource loadAsResource(final String filename);

    Resource loadSmallAsResource(final String filename);

    Resource loadMediumAsResource(final String filename);

    boolean deleteByName(final String name);

}
