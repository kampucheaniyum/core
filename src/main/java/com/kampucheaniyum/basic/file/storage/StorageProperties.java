package com.kampucheaniyum.basic.file.storage;

import lombok.Getter;
import lombok.Setter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
@Getter
@Setter
public class StorageProperties {

    @Value("${upload-dir:/opt/upload}")
    private String location;

    @Value("${small-upload-dir:/opt/upload/small}")
    private String smallLocation;

    @Value("${small-width:250}")
    private int smallWidth;

    @Value("${small-height:250}")
    private int smallHeight;

    @Value("${medium-upload-dir:/opt/upload/medium}")
    private String mediumLocation;

    @Value("${medium-width:500}")
    private int mediumWidth;

    @Value("${medium-height:500}")
    private int mediumHeight;

}
