package com.kampucheaniyum.basic.file.utils;

import com.kampucheaniyum.basic.file.data.FileType;
import org.springframework.util.StringUtils;

import java.util.Map;
import java.util.Set;

import static com.kampucheaniyum.basic.file.data.FileType.OTHER;

public class FileNameUtil {

    private static final Set<String> IMAGE_TYPE = Set.of("png", "jpg", "gif", "jpeg", "bmp");
    private static final Set<String> VIDEO_TYPE = Set.of("mp4", "kvm", "flv", "avi");
    private static final Set<String> SOUND_TYPE = Set.of("wav", "mp3", "amr");
    private static final Set<String> DOCUMENT_TYPE = Set.of("doc", "docx", "xls", "xlsx", "ppt", "pptx", "txt", "pdf");
    private static final Map<Set<String>, FileType> MAP = Map.of(
            IMAGE_TYPE, FileType.IMAGE,
            VIDEO_TYPE, FileType.VIDEO,
            SOUND_TYPE, FileType.SOUND,
            DOCUMENT_TYPE, FileType.DOCUMENT
    );

    public static FileType getFileType(final String fileName) {
        final var ext = getFileExtension(fileName);
        if (ext == null || ext.isBlank()) return OTHER;
        return MAP.entrySet()
                .stream()
                .filter(setFileTypeEntry -> setFileTypeEntry.getKey().contains(ext))
                .findFirst()
                .map(Map.Entry::getValue)
                .orElse(OTHER);
    }

    public static String getFileExtension(final String fileName) {
        if (fileName == null || fileName.isBlank()) return null;
        return StringUtils.getFilenameExtension(fileName);
    }
}
