package com.kampucheaniyum.portfolio.organization.controller;

import com.kampucheaniyum.portfolio.organization.domain.DetailType;
import com.kampucheaniyum.portfolio.organization.domain.OrganizationDetail;
import com.kampucheaniyum.portfolio.organization.repository.OrganizationDetailRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
@RequiredArgsConstructor
@RequestMapping("/api/organization")
public class OrganizationController {

    private final OrganizationDetailRepository repository;

    @GetMapping("/about")
    public OrganizationDetail getAbout() {
        return repository.findByDetailType(DetailType.ABOUT);
    }

    @GetMapping("/history")
    public OrganizationDetail getHistory() {
        return repository.findByDetailType(DetailType.HISTORY);
    }

    @PutMapping
    public OrganizationDetail update(@RequestBody OrganizationDetail organizationDetail) {
        if (organizationDetail.getContent() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "content is required");
        }

        if (organizationDetail.getDetailType() == null) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "detail type is required");
        }

        final var existingOrg = repository.findByDetailType(organizationDetail.getDetailType());
        if (existingOrg == null) {
            return repository.save(organizationDetail);
        }
        existingOrg.setContent(organizationDetail.getContent());
        return repository.save(existingOrg);
    }
}
