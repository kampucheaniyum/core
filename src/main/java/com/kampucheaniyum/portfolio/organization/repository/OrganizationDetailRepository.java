package com.kampucheaniyum.portfolio.organization.repository;

import com.kampucheaniyum.portfolio.organization.domain.DetailType;
import com.kampucheaniyum.portfolio.organization.domain.OrganizationDetail;
import org.springframework.data.jpa.repository.JpaRepository;

public interface OrganizationDetailRepository extends JpaRepository<OrganizationDetail, Long> {
    OrganizationDetail findByDetailType(DetailType detailType);
}