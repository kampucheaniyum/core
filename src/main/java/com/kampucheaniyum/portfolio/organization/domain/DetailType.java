package com.kampucheaniyum.portfolio.organization.domain;

public enum DetailType {
    ABOUT,
    HISTORY
}
