package com.kampucheaniyum.portfolio.organization.domain;

import com.kampucheaniyum.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@Entity
@Getter
@Setter
public class OrganizationDetail extends VersionEntity {

    @Column(unique = true)
    @Enumerated(EnumType.STRING)
    private DetailType detailType;

    @Column(columnDefinition = "longtext")
    private String content;
}
