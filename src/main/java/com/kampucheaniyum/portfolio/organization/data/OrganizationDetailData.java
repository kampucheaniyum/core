package com.kampucheaniyum.portfolio.organization.data;

import com.kampucheaniyum.portfolio.organization.domain.DetailType;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrganizationDetailData {
    private DetailType detailType;
    private String content;
}
