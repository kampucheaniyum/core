package com.kampucheaniyum.portfolio.content.controller;

import com.kampucheaniyum.basic.notification.service.NotificationService;
import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.persistence.service.EntityDataMapper;
import com.kampucheaniyum.portfolio.content.data.ContentData;
import com.kampucheaniyum.portfolio.content.data.mapper.ContentMapper;
import com.kampucheaniyum.portfolio.content.domain.Content;
import com.kampucheaniyum.portfolio.content.domain.ContentType;
import com.kampucheaniyum.portfolio.content.repository.ContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/content")
public class ContentController {

    private final ContentRepository repository;
    private final ContentMapper mapper;
    private final EntityDataMapper dataMapper;
    private final NotificationService notificationService;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ContentData create(@Valid @RequestBody ContentData data) {
        var contentEntity = mapper.map(data);
        var content = repository.save(contentEntity);
        notificationService.contentPublished(content);
        return mapper.apply(content);
    }

    @GetMapping("{id}")
    public ContentData getOne(@PathVariable Long id) {
        return repository.findById(id)
                .map(mapper)
                .orElseThrow(() -> new ResourceNotFoundException(Content.class, id));
    }

    @GetMapping
    public Page<ContentData> getAll(Long categoryId, ContentType contentType, Pageable pageable) {
        if (categoryId != null && contentType != null) {
            return repository.findAllByCategoryIdAndContentType(categoryId, contentType, pageable)
                    .map(mapper);
        } else if (categoryId == null && contentType != null) {
            return repository.findAllByContentType(contentType, pageable)
                    .map(mapper);
        } else if (categoryId != null) {
            return repository.findAllByCategoryId(categoryId, pageable)
                    .map(mapper);
        } else {
            return repository.findAll(pageable)
                    .map(mapper);
        }
    }

    @GetMapping("/search")
    public Page<ContentData> search(@RequestParam String key, Pageable pageable) {
        var searchKey = "%" + key + "%";
        return repository.findAllByTitleLikeOrContentLike(searchKey, searchKey, pageable).map(mapper);
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ContentData update(@PathVariable Long id, @RequestBody ContentData data) {
        var target = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Content.class, id));
        var source = mapper.map(data);
        var newContent = dataMapper.mapObject(source, target, Content.class);
        repository.save(newContent);
        return mapper.apply(newContent);
    }

    @PostMapping("{id}/priority")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public ContentData increasePriority(@PathVariable Long id, @RequestParam(defaultValue = "increase") String command) {
        var target = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Content.class, id));
        if ("increase".equalsIgnoreCase(command)) {
            target.increasePriority();
        } else if ("decrease".equalsIgnoreCase(command)) {
            target.decreasePriority();
        } else {
            throw new BadRequestException("command can only be `increase` or `decrease`");
        }
        var newContent = repository.save(target);
        return mapper.apply(newContent);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public boolean delete(@PathVariable Long id) {
        repository.deleteById(id);
        return true;
    }
}
