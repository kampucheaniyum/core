package com.kampucheaniyum.portfolio.content.controller;

import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.portfolio.content.data.CategoryData;
import com.kampucheaniyum.portfolio.content.data.mapper.CategoryMapper;
import com.kampucheaniyum.portfolio.content.domain.Category;
import com.kampucheaniyum.portfolio.content.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/category")
public class CategoryController {

    private final CategoryRepository repository;
    private final CategoryMapper mapper;

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public CategoryData create(@Valid @RequestBody CategoryData data) {
        var category = mapper.map(data);
        return mapper.apply(repository.save(category));
    }

    @GetMapping
    public List<CategoryData> getAll() {
        var sort = Sort.by(Sort.Direction.DESC, "priority");
        return repository.findAll(sort).stream().map(mapper).collect(Collectors.toList());
    }

    @GetMapping("{id}")
    public CategoryData getOne(@PathVariable Long id) {
        return repository.findById(id)
                .map(mapper)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class, id));
    }

    @PutMapping("{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public CategoryData update(@PathVariable Long id, @RequestBody CategoryData data) {
        var category = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class, id));
        if (StringUtils.hasText(data.getName())) {
            category.setName(data.getName());
        }
        if (data.getPriority() != null) {
            category.setPriority(data.getPriority());
        }
        repository.save(category);
        return mapper.apply(category);
    }

    @PostMapping("{id}/priority")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public CategoryData increasePriority(@PathVariable Long id, @RequestParam(defaultValue = "increase") String command) {
        var target = repository.findById(id)
                .orElseThrow(() -> new ResourceNotFoundException(Category.class, id));
        if ("increase".equalsIgnoreCase(command)) {
            target.increasePriority();
        } else if ("decrease".equalsIgnoreCase(command)) {
            target.decreasePriority();
        } else {
            throw new BadRequestException("command can only be `increase` or `decrease`");
        }
        var newContent = repository.save(target);
        return mapper.apply(newContent);
    }

    @DeleteMapping("{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public boolean delete(@PathVariable Long id) {
        repository.deleteById(id);
        return true;
    }
}
