package com.kampucheaniyum.portfolio.content.service;

import com.kampucheaniyum.portfolio.content.domain.Content;
import com.kampucheaniyum.portfolio.content.repository.ContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ContentService {

    private final ContentRepository repository;

    public Page<Content> search() {
        return null;
    }
}
