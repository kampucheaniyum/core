package com.kampucheaniyum.portfolio.content.repository;

import com.kampucheaniyum.portfolio.content.domain.Content;
import com.kampucheaniyum.portfolio.content.domain.ContentType;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContentRepository extends JpaRepository<Content, Long> {

    Page<Content> findAllByTitleLikeOrContentLike(String title, String content, Pageable pageable);

    Page<Content> findAllByCategoryId(Long categoryId, Pageable pageable);

    Page<Content> findAllByContentType(ContentType contentType, Pageable pageable);

    Page<Content> findAllByCategoryIdAndContentType(Long categoryId, ContentType contentType, Pageable pageable);
}