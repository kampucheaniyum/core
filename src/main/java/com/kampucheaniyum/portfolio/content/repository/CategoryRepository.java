package com.kampucheaniyum.portfolio.content.repository;

import com.kampucheaniyum.portfolio.content.domain.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {
}