package com.kampucheaniyum.portfolio.content.data.mapper;

import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.persistence.mapper.DataMapper;
import com.kampucheaniyum.portfolio.content.data.ContentData;
import com.kampucheaniyum.portfolio.content.domain.Category;
import com.kampucheaniyum.portfolio.content.domain.Content;
import com.kampucheaniyum.portfolio.content.repository.CategoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class ContentMapper extends DataMapper<Content, ContentData> {

    private final CategoryRepository categoryRepository;

    @Override
    public Content map(ContentData contentData) {
        var content = super.map(contentData);
        if (contentData.getCategoryId() != null) {
            var category = categoryRepository.findById(contentData.getCategoryId())
                    .orElseThrow(() -> new ResourceNotFoundException(Category.class, contentData.getCategoryId()));
            content.setCategory(category);
        }
        return content;
    }
}
