package com.kampucheaniyum.portfolio.content.data.mapper;

import com.kampucheaniyum.persistence.mapper.DataMapper;
import com.kampucheaniyum.portfolio.content.data.CategoryData;
import com.kampucheaniyum.portfolio.content.domain.Category;
import org.springframework.stereotype.Component;

@Component
public class CategoryMapper extends DataMapper<Category, CategoryData> {
}
