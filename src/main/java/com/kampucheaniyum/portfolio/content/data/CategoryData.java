package com.kampucheaniyum.portfolio.content.data;

import com.kampucheaniyum.persistence.data.AuditingEntityData;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;

@Getter
@Setter
public class CategoryData extends AuditingEntityData {
    @NotBlank
    private String name;
    private Integer priority;
}
