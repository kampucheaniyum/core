package com.kampucheaniyum.portfolio.content.data;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.kampucheaniyum.persistence.data.AuditingEntityData;
import com.kampucheaniyum.portfolio.content.domain.ContentType;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class ContentData extends AuditingEntityData {
    @NotNull
    private Long categoryId;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String categoryName;
    @NotBlank
    private String title;
    private String content;
    private String mediaUrl;
    @NotNull
    private ContentType contentType;
    private int priority;
}
