package com.kampucheaniyum.portfolio.content.domain;

import com.kampucheaniyum.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Content extends AuditingEntity {

    @ManyToOne
    @JoinColumn(name = "category_id")
    private Category category;

    @Column(nullable = false)
    private String title;

    @Column(columnDefinition = "longtext")
    private String content;

    private String mediaUrl;

    @Enumerated(EnumType.STRING)
    private ContentType contentType;

    private int priority;

    public void increasePriority() {
        this.priority += 1;
    }

    public void decreasePriority() {
        this.priority -= 1;
    }
}
