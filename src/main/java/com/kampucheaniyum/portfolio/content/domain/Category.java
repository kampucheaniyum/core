package com.kampucheaniyum.portfolio.content.domain;

import com.kampucheaniyum.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@Accessors(chain = true)
@Table(name = "category")
@SQLDelete(sql = "update category set deleted = 1 where id = ? and version = ?")
@Where(clause = "deleted = 0")
public class Category extends AuditingEntity {

    private String name;

    private int priority;

    private boolean deleted;

    public void increasePriority() {
        this.priority += 1;
    }

    public void decreasePriority() {
        this.priority -= 1;
    }
}
