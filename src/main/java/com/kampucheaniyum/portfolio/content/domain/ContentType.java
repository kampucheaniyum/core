package com.kampucheaniyum.portfolio.content.domain;

public enum ContentType {
    TEXT,
    VOICE,
    VIDEO
}
