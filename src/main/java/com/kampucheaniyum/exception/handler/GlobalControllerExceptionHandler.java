package com.kampucheaniyum.exception.handler;

import com.kampucheaniyum.basic.file.storage.StorageFileNotFoundException;
import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.exception.ForbiddenException;
import com.kampucheaniyum.exception.PasswordInvalidException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.exception.UnAuthorizeException;
import com.kampucheaniyum.exception.data.ExceptionData;
import com.kampucheaniyum.exception.data.ParamError;
import org.springframework.context.support.DefaultMessageSourceResolvable;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import java.util.Arrays;
import java.util.stream.Collectors;

@ControllerAdvice
public class GlobalControllerExceptionHandler {

    @ExceptionHandler(DataIntegrityViolationException.class)
    protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
        var message = ex.getMessage();
        if (message != null && message.contains("unique_username")) {
            message = "user name duplicated";
        }
        var data = new ExceptionData()
                .setCode(HttpStatus.CONFLICT.value())
                .setMessage(message);
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.CONFLICT);
    }

    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<Object> handleNotFound(RuntimeException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.NOT_FOUND.value())
                .setMessage(ex.getMessage());
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(ForbiddenException.class)
    protected ResponseEntity<Object> handleForbidden(RuntimeException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.FORBIDDEN.value())
                .setMessage(ex.getMessage());
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({BadRequestException.class, PasswordInvalidException.class})
    protected ResponseEntity<Object> handleBadRequest(BadRequestException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.BAD_REQUEST.value())
                .setErrors(ex.getErrors())
                .setMessage(ex.getMessage());
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    protected ResponseEntity<Object> handleBadRequest(MissingServletRequestParameterException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.BAD_REQUEST.value())
                .setMessage(ex.getMessage());
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleDefaultBadRequest(MethodArgumentNotValidException ex, WebRequest request) {
        var errors = ex.getAllErrors()
                .stream()
                .map(this::toParamError)
                .collect(Collectors.toList());
        var data = new ExceptionData()
                .setCode(HttpStatus.BAD_REQUEST.value())
                .setErrors(errors)
                .setMessage("Invalid JSON input");
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    protected ResponseEntity<Object> handleDefaultBadRequest(HttpMessageNotReadableException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.BAD_REQUEST.value())
                .setMessage("Invalid JSON input");
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(UnAuthorizeException.class)
    protected ResponseEntity<Object> handleUnAuthorize(UnAuthorizeException ex, WebRequest request) {
        var data = new ExceptionData()
                .setCode(HttpStatus.UNAUTHORIZED.value())
                .setMessage(ex.getMessage());
        return new ResponseEntity<>(data, new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    private ParamError toParamError(ObjectError error) {
        if (error.getArguments() == null) {
            return null;
        }
        var param = Arrays.stream(error.getArguments())
                .filter(o -> o instanceof DefaultMessageSourceResolvable)
                .map(o -> (DefaultMessageSourceResolvable) o)
                .map(DefaultMessageSourceResolvable::getDefaultMessage)
                .collect(Collectors.joining(","));
        return new ParamError(param, error.getDefaultMessage());
    }

    @ExceptionHandler(StorageFileNotFoundException.class)
    public ResponseEntity<?> handleStorageFileNotFound(StorageFileNotFoundException exc) {
        return ResponseEntity.notFound().build();
    }
}