package com.kampucheaniyum.exception;

import com.kampucheaniyum.exception.data.ParamError;
import lombok.Getter;

import java.util.List;

@Getter
public class BadRequestException extends RuntimeException {
    private final List<ParamError> errors;
    private final String message;

    public BadRequestException(String message) {
        super(message);
        this.message = message;
        this.errors = List.of();
    }

    public BadRequestException(String message, List<ParamError> errors) {
        super(message);
        this.errors = errors;
        this.message = message;
    }

    public BadRequestException(String message, String key1, String value1) {
        super(message);
        this.message = message;
        var error = new ParamError(key1, value1);
        this.errors = List.of(error);
    }

    public BadRequestException(String message,
                               String key1,
                               String value1,
                               String key2,
                               String value2) {
        super(message);
        this.message = message;
        var error1 = new ParamError(key1, value1);
        var error2 = new ParamError(key2, value2);
        this.errors = List.of(error1, error2);
    }
}
