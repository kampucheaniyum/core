package com.kampucheaniyum.exception;

public class UnAuthorizeException extends RuntimeException {
    public UnAuthorizeException() {
        super("request unauthorized");
    }
}
