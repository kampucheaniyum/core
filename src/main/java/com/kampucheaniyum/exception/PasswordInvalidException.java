package com.kampucheaniyum.exception;

public class PasswordInvalidException extends BadRequestException {

    public PasswordInvalidException() {
        super("Old password is invalid", "password", "Old Password invalid");
    }
}
