package com.kampucheaniyum.user.authentication.controller;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.kampucheaniyum.appconfiguration.utils.JwtUtil;
import com.kampucheaniyum.appconfiguration.utils.TokenConfig;
import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.user.authentication.data.AuthenticationData;
import com.kampucheaniyum.user.authentication.data.AuthenticationRequest;
import com.kampucheaniyum.user.authentication.data.TokenData;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.domain.UserRole;
import com.kampucheaniyum.user.authentication.service.AppUserService;
import com.kampucheaniyum.user.authentication.service.UserDetailsServiceImpl;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@RestController
@RequestMapping("auth")
@RequiredArgsConstructor
public class AuthenticationController {

    private final AuthenticationManager authenticationManager;
    private final UserDetailsServiceImpl userDetailsService;
    private final AppUserService appUserService;
    private final TokenConfig tokenConfig;
    private final JwtUtil jwtUtil;

    @PostMapping("token")
    public AuthenticationData createAuthenticationToken(@RequestBody @Valid AuthenticationRequest request) {
        var authenticationToken = new UsernamePasswordAuthenticationToken(request.getUsername(), request.getPassword());
        authenticationManager.authenticate(authenticationToken);
        var userDetails = userDetailsService.loadUserByUsername(request.getUsername());
        return jwtUtil.generateToken(userDetails);
    }

    @PostMapping("reset-password")
    public ResponseEntity<?> resetPassword(@RequestBody @Valid AuthenticationRequest request) {
        try {
            var success = userDetailsService.resetPassword(request.getUsername(), request.getPassword());
            if (success) {
                return new ResponseEntity<>(HttpStatus.OK);
            }
        } catch (Exception e) {
            log.error("Cannot reset password");
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/token/firebase")
    public AuthenticationData createTokenWithFirebaseToken(HttpServletRequest request, @RequestBody @Valid TokenData tokenData) {
        try {
            var decodedToken = FirebaseAuth.getInstance().verifyIdToken(tokenData.getToken());
            try {
                var userdetails = userDetailsService.loadUserByUsername(decodedToken.getUid());
                return jwtUtil.generateToken(userdetails);
            } catch (ResourceNotFoundException e) {
                String mobile = null;
                if (StringUtils.hasText(decodedToken.getUid())) {
                    var userRecord = FirebaseAuth.getInstance().getUser(decodedToken.getUid());
                    mobile = userRecord == null ? null : userRecord.getPhoneNumber();
                }
                var appUser = new AppUser()
                        .setFirebaseUser(true)
                        .setEmail(decodedToken.getEmail())
                        .setMobile(mobile)
                        .setUsername(decodedToken.getUid())
                        .setDisplayName(decodedToken.getName())
                        .setPassword("123!@#")
                        .setRole(UserRole.USER)
                        .setPhotoUrl(decodedToken.getPicture());
                appUserService.createUser(appUser, request);
                var userdetails = userDetailsService.loadUserByUsername(decodedToken.getUid());
                return jwtUtil.generateToken(userdetails);
            }
        } catch (FirebaseAuthException e) {
            throw new BadRequestException("Token invalid");
        }
    }

    @PostMapping("refresh-token")
    public ResponseEntity<?> refreshToken(@RequestBody @Valid TokenData tokenData) {

        // get claims
        Claims claims;
        try {
            claims = Jwts.parser()
                    .setSigningKey(tokenConfig.getSecret())
                    .parseClaimsJws(tokenData.getToken())
                    .getBody();
        } catch (ExpiredJwtException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Refresh token is expired");
        } catch (Exception e) {
            log.error(e.getMessage());
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Invalid refresh token");
        }

        // validate token type
        if (!claims.containsKey("refreshToken")) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, "Provided token is not refresh token");
        }

        var expectedMap = getMapFromIoJsonWebTokenClaims(claims);
        var subject = expectedMap.get("sub").toString();

        // generate refresh token
        var refreshToken = jwtUtil.generateRefreshToken(expectedMap, subject);

        // generate token
        expectedMap.remove("refreshToken");
        var authToken = jwtUtil.generateToken(expectedMap, subject);

        return ResponseEntity.ok(new AuthenticationData(authToken, refreshToken));
    }

    private Map<String, Object> getMapFromIoJsonWebTokenClaims(Claims claims) {
        return new HashMap<>(claims);
    }

}
