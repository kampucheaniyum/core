package com.kampucheaniyum.user.authentication.controller;

import com.kampucheaniyum.appconfiguration.utils.ApplicationSecurityContext;
import com.kampucheaniyum.exception.ForbiddenException;
import com.kampucheaniyum.exception.UnAuthorizeException;
import com.kampucheaniyum.user.authentication.data.UserData;
import com.kampucheaniyum.user.authentication.data.UserDataMapper;
import com.kampucheaniyum.user.authentication.service.AppUserService;
import com.kampucheaniyum.user.member.itextkh.service.ItextService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@RestController
@RequestMapping("api/users")
@RequiredArgsConstructor
public class AppUserController {

    private final AppUserService service;
    private final UserDataMapper mapper;
    private final ApplicationSecurityContext context;
    private final ItextService itextService;

    @GetMapping
    public Page<UserData> getAllUser(Pageable pageable) {
        return service.getAll(pageable).map(mapper);
    }

    @GetMapping("{id}")
    public UserData getUserById(@PathVariable("id") final Long id) {
        return mapper.apply(service.getById(id));
    }

    @GetMapping("current")
    public UserData getCurrentUser() {
        final var user = context.authenticatedUser();
        if (user == null) {
            throw new ForbiddenException("unauthorized request");
        }
        return mapper.apply(user);
    }

    @GetMapping(value = "/card", produces = MediaType.APPLICATION_PDF_VALUE)
    @ResponseBody
    public byte[] getIdCard() throws Exception {
        final var user = context.authenticatedUser();
        if (user == null) {
            throw new UnAuthorizeException();
        }
        return itextService.getUserIdCard(user);
    }

    @PutMapping("changePassword")
    public UserData changePassword(@RequestParam("old") final String oldPassword,
                                   @RequestParam("new") final String newPassword) {
        return mapper.apply(service.changePassword(context.authenticatedUser(), oldPassword, newPassword));
    }

    @PutMapping("{id}")
    public UserData updateUser(@NotNull @PathVariable final Long id, @RequestBody final UserData data) {
        return mapper.apply(service.updateUser(id, mapper.map(data)));
    }

    @PostMapping
    public UserData createUser(HttpServletRequest request, @Valid @RequestBody final UserData data) {
        return mapper.apply(service.createUser(mapper.map(data), request));
    }

    @DeleteMapping("{id}")
    public boolean deleteUser(@NotNull @PathVariable("id") final Long id) {
        if (id.equals(context.authenticatedUser().getId())) {
            throw new ForbiddenException("cannot delete current user");
        }
        if (id == 1L) {
            throw new ForbiddenException("cannot delete admin user");
        }
        service.deleteById(id);
        return true;
    }
}
