package com.kampucheaniyum.user.authentication.controller;

import com.kampucheaniyum.appconfiguration.utils.ApplicationSecurityContext;
import com.kampucheaniyum.exception.UnAuthorizeException;
import com.kampucheaniyum.user.authentication.domain.FireBaseToken;
import com.kampucheaniyum.user.authentication.repository.FireBaseTokenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;

@RestController
@RequestMapping("api/firebase")
@RequiredArgsConstructor
public class FirebaseTokenController {

    private final FireBaseTokenRepository repository;
    private final ApplicationSecurityContext context;

    @PostMapping("token")
    @PreAuthorize("hasAnyAuthority('USER')")
    public FireBaseToken addToken(@RequestParam @Valid @NotBlank String token) {
        var user = context.authenticatedUser();
        if (user == null) {
            throw new UnAuthorizeException();
        }
        var firebaseToken = new FireBaseToken().setToken(token);
        return repository.save(firebaseToken);
    }
}
