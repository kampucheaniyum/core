package com.kampucheaniyum.user.authentication.repository;

import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.domain.FireBaseToken;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FireBaseTokenRepository extends JpaRepository<FireBaseToken, Long> {

    List<FireBaseToken> findAllByCreatedBy(AppUser createdBy);
}
