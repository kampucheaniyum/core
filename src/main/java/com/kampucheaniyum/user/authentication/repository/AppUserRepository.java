package com.kampucheaniyum.user.authentication.repository;

import com.kampucheaniyum.user.authentication.domain.AppUser;
import org.springframework.data.jpa.repository.JpaRepository;
import java.util.Optional;

public interface AppUserRepository extends JpaRepository<AppUser, Long> {

    Optional<AppUser> findByUsername(final String name);

    Optional<AppUser> findByEmail(final String email);

    Optional<AppUser> findByMobile(final String mobile);

}
