package com.kampucheaniyum.user.authentication.domain;

public enum UserRole {
    ADMIN,
    EDITOR,
    USER
}
