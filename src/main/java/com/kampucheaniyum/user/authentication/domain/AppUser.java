package com.kampucheaniyum.user.authentication.domain;

import com.kampucheaniyum.basic.utils.CustomStringUtils;
import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.persistence.domain.VersionEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import org.hibernate.annotations.SQLDelete;
import org.hibernate.annotations.Where;
import org.springframework.util.StringUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@Table(name = "users", uniqueConstraints = @UniqueConstraint(name = "unique_username", columnNames = "username"))
@Accessors(chain = true)
@Where(clause = "status <> 'DELETED'")
@SQLDelete(sql = "update users set status = 'DELETED' where id = ? and version = ?")
public class AppUser extends VersionEntity {

    @Column(name = "created_at", updatable = false)
    private LocalDateTime createdAt = LocalDateTime.now();

    @Column(name = "username", unique = true)
    private String username;

    private String displayName;

    private String email;

    private String mobile;

    @Column(nullable = false)
    private String password;

    @Enumerated(EnumType.STRING)
    private UserStatus status;

    @Enumerated(EnumType.STRING)
    private UserRole role;

    private boolean firebaseUser;

    private String photoUrl;

    // todo: generate with a format
    private String cardNumber;

    private String qrCode;

    public String getDisplayName() {
        return StringUtils.hasText(displayName) ? displayName : StringUtils.hasText(email) ? email : mobile;
    }

    @PrePersist
    private void prePersist() {
        if (status == null) {
            status = UserStatus.ACTIVE;
        }
        if (!StringUtils.hasText(email) && !StringUtils.hasText(mobile)) {
            throw new BadRequestException(
                    "email or mobile must present",
                    "email",
                    "email or mobile must present",
                    "mobile", "email or mobile must present");
        }
        if (!StringUtils.hasText(username)) {
            username = StringUtils.hasLength(email) ? email : mobile;
        }
        if (role == null) {
            role = UserRole.USER;
        }
        if (!StringUtils.hasText(displayName)) {
            displayName = StringUtils.hasText(email) ? email : mobile;
        }
        cardNumber = CustomStringUtils.padLeftZeros("" + System.currentTimeMillis(), 16);
    }
}
