package com.kampucheaniyum.user.authentication.domain;

public enum UserStatus {
    ACTIVE,
    BLOCKED,
    DELETED
}
