package com.kampucheaniyum.user.authentication.data;

import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class TokenData {
    @NotBlank
    private String token;
}
