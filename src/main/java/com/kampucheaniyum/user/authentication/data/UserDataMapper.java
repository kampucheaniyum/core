package com.kampucheaniyum.user.authentication.data;

import com.kampucheaniyum.persistence.mapper.DataMapper;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import lombok.RequiredArgsConstructor;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@RequiredArgsConstructor
public class UserDataMapper extends DataMapper<AppUser, UserData> {

    final PasswordEncoder encoder;

    @Override
    public UserData apply(AppUser appUser) {
        var data = super.apply(appUser);
        data.setUserName(appUser.getUsername());
        return data;
    }

    @Override
    public AppUser map(UserData data) {
        final var user = super.map(data);
        if (StringUtils.hasText(data.getPassword())) {
            user.setPassword(encoder.encode(data.getPassword()));
        }
        return user;
    }
}
