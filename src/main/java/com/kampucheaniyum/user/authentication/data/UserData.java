package com.kampucheaniyum.user.authentication.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.kampucheaniyum.user.authentication.domain.UserRole;
import lombok.AccessLevel;
import lombok.Data;
import lombok.experimental.FieldDefaults;

import javax.validation.constraints.Email;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Data
@FieldDefaults(level = AccessLevel.PRIVATE)
@JsonIgnoreProperties(value = "password", allowSetters = true)
public class UserData {
    Long id;
    String userName;
    @JsonFormat(pattern = "yyyy-MM-dd")
    LocalDateTime createdAt;
    @Email
    String email;
    String mobile;
    String password;
    UserRole role;
    String displayName;
    String photoUrl;
    String cardNumber;
    String qrCode;
}
