package com.kampucheaniyum.user.authentication.service;

import com.kampucheaniyum.exception.PasswordInvalidException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.persistence.service.EntityDataMapper;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Service
@RequiredArgsConstructor
@Transactional
public class AppUserService {

    private final AppUserRepository repository;
    private final PasswordEncoder encoder;
    private final EntityDataMapper entityDataMapper;

    public AppUser getUserByUserName(final String name) {
        return repository.findByUsername(name).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, name));
    }

    public AppUser getUserByEmail(@Email @NotNull final String email) {
        return repository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, email));
    }

    public AppUser getById(Long id) {
        return repository.findById(id).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, id));
    }

    public Page<AppUser> getAll(Pageable pageable) {
        return repository.findAll(pageable);
    }

    public void deleteById(Long id) {
        repository.deleteById(id);
    }

    public AppUser changePassword(final AppUser user, final String oldPass, final String newPass) {
        if (user != null && encoder.matches(oldPass, user.getPassword())) {
            assert user.getId() != null;
            final var appUser = repository.findById(user.getId()).orElseThrow();
            appUser.setPassword(encoder.encode(newPass));
            return repository.save(appUser);
        }
        throw new PasswordInvalidException();
    }

    public AppUser updateUser(Long id, final AppUser sourceUser) {
        final var targetUser = getById(id);
        final var password = targetUser.getPassword();
        final var user = entityDataMapper.mapObject(sourceUser, targetUser, AppUser.class);
        if (!StringUtils.hasText(sourceUser.getPassword())) { //keep password
            user.setPassword(password);
        }
        return repository.save(user);

    }

    public AppUser createUser(final AppUser appUser, HttpServletRequest request) {
        final var user = repository.save(appUser);
        final var url = request.getRequestURL()
                .toString()
                .replace("/api/users", "");
        user.setQrCode(url + "/user/" + appUser.getId() + "/profile");
        return user;
    }
}
