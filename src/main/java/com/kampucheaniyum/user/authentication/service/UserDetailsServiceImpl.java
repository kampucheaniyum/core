package com.kampucheaniyum.user.authentication.service;

import com.kampucheaniyum.user.authentication.domain.UserRole;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserService service;

    @Override
    @Transactional
    public UserAuthentication loadUserByUsername(final String name) throws UsernameNotFoundException {
        final var appUser = service.getUserByUserName(name);
        return new UserAuthentication(appUser.getUsername(), appUser.getPassword(), getAuthorities(appUser.getRole()), appUser);
    }

    private List<GrantedAuthority> getAuthorities(UserRole role) {
        return List.of(new SimpleGrantedAuthority(role.name()));
    }

    public Boolean resetPassword(final String name, final String newPassword) {
        try {
            final var appUser = isPhoneNumber(name)
                    ? service.getUserPhoneNumber(name)
                    : isEmail(name)
                    ? service.getUserByEmail(name)
                    : service.getUserByUserName(name);

            if (appUser == null) {
                return false;
            }

            return service.resetPassword(appUser, newPassword);

        } catch (Exception e) {
            return false;
        }
    }

    private boolean isPhoneNumber(String name) {
        return name.matches("^[0-9]{9,10}$");
    }

    private boolean isEmail(String name) {
        return name.matches("^[\\w-\\.]+@([\\w-]+\\.)+[\\w-]{2,4}$");
    }
}
