package com.kampucheaniyum.user.authentication.service;

import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.repository.AppUserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private final AppUserRepository repository;
    private final PasswordEncoder encoder;

    public AppUser getUserByUserName(final String name) {
        return repository.findByUsername(name).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, name));
    }

    public AppUser getUserByEmail(final String email) {
        return repository.findByEmail(email).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, email));
    }

    public AppUser getUserPhoneNumber(final String phone) {
        return repository.findByMobile(phone).orElseThrow(() -> new ResourceNotFoundException(AppUser.class, phone));
    }

    public boolean resetPassword(final AppUser appUser, final String newPassword) {
        appUser.setPassword(encoder.encode(newPassword));
        repository.save(appUser);
        return true;

    }
}
