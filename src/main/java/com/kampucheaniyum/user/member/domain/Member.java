package com.kampucheaniyum.user.member.domain;

import com.kampucheaniyum.persistence.domain.AuditingEntity;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.PrePersist;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Accessors(chain = true)
public class Member extends AuditingEntity {

    @Enumerated(EnumType.STRING)
    private MemberType type;

    private String khmerFullName;

    private String englishFullName;

    private String idCardNo;

    private String idCardImageFront;

    private String idCardImageBack;

    @Enumerated(EnumType.STRING)
    private Sex sex;

    private LocalDate dateOfBirth;

    @Column(columnDefinition = "TEXT")
    private String rejectReason;

    @Embedded
    private Address address;

    @Enumerated(EnumType.STRING)
    private MemberStatus status;

    @PrePersist
    private void prePersist() {
        status = MemberStatus.PENDING;
    }
}
