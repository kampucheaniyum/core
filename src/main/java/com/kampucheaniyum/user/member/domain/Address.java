package com.kampucheaniyum.user.member.domain;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.NotBlank;

@Setter
@Getter
@Embeddable
public class Address {

    private String addressLIne;

    @NotBlank
    @Column(nullable = false)
    private String village;

    @NotBlank
    @Column(nullable = false)
    private String commune;

    @NotBlank
    @Column(nullable = false)
    private String district;

    @NotBlank
    @Column(nullable = false)
    private String province;
}
