package com.kampucheaniyum.user.member.domain;

public enum MemberStatus {
    PENDING,
    APPROVED,
    REJECTED
}
