package com.kampucheaniyum.user.member.domain;

public enum MemberType {
    SUPPORTER,
    ACTIVIST,
    SENATOR
}
