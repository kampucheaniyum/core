package com.kampucheaniyum.user.member.domain;

public enum Sex {
    MALE,
    FEMALE,
    OTHER
}
