package com.kampucheaniyum.user.member.itextkh.service;

import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DateUtilHelper {

    private static final List<String> MONTHS = List.of("មករា", "កុម្ភៈ", "មីនា", "មេសា", "ឧសភា",
            "មិថុនា", "កក្កដា", "សីហា", "កញ្ញា", "តុលា", "វិច្ឆិកា", "ធ្នូ");

    private static final List<String> NUMBERS = List.of("០", "១", "២", "៣", "៤", "៥", "៦", "៧", "៨", "៩");

    public String getMonthAsKhmer(final int month) {
        return MONTHS.get(month - 1);
    }

    public String getNumberAsKhmer(final String numberAsString) {
        StringBuilder khmerNumber = new StringBuilder();
        for( var i=0; i < numberAsString.length(); i++ ) {
            khmerNumber.append(NUMBERS.get(Integer.parseInt(String.valueOf(numberAsString.charAt(i)))));
        }
        return khmerNumber.toString();
    }
}
