package com.kampucheaniyum.user.member.itextkh.render;

import com.itextpdf.text.pdf.BidiLine;
import com.itextpdf.text.pdf.languages.LanguageProcessor;

public class KhmerLigaturizer implements LanguageProcessor {

    public KhmerLigaturizer() {
    }

    public String process(String s) {
        UnicodeRender khmerRender = new UnicodeRender();
        return BidiLine.processLTR(khmerRender.render(s), 2, 0);
    }

    public boolean isRTL() {
        return false;
    }
}