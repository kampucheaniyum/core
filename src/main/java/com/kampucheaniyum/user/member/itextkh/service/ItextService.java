package com.kampucheaniyum.user.member.itextkh.service;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BarcodeQRCode;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.repository.AppUserRepository;
import com.kampucheaniyum.user.member.itextkh.render.KhmerLigaturizer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;

@Service
public class ItextService {

    @Autowired
    private AppUserRepository userRepository;

    @Autowired
    private DateUtilHelper dateUtilHelper;

    private final static String KH_FONT_PATH = "src/main/resources/fonts/kh_battambang.ttf";
    private final static String LOGO_PATH = "src/main/resources/files/logo.jpg";
    private final static String USER_PROFILE = "src/main/resources/files/user-profile.png";
    private final static String OUTPUT_FILE = "tmp-card.pdf";

    private final BaseFont bfComic = BaseFont.createFont(KH_FONT_PATH, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);

    private static final int NORMAL_FONT = 7;

    public ItextService() throws IOException, DocumentException {
    }

    private void header(Document document,
                        KhmerLigaturizer kh,
                        AppUser appUser) throws Exception {
        PdfPTable table1 = new PdfPTable(1);
        table1.setHorizontalAlignment(Element.ALIGN_CENTER);
        table1.setWidthPercentage(100);
        table1.setHorizontalAlignment(Element.ALIGN_LEFT);
        table1.setWidths(new int[]{10});
        table1.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        // company logo
        Image logo = Image.getInstance(LOGO_PATH);
        logo.scaleAbsolute(50, 50);
        logo.setWidthPercentage(10);

        final var cellLogo = new PdfPCell(logo);
        cellLogo.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellLogo.setBorder(Rectangle.NO_BORDER);

        // user profile
        Image userProfile;
        try {
            userProfile = Image.getInstance(new URL(appUser.getPhotoUrl()));
        } catch (final Exception e) {
            userProfile = Image.getInstance(USER_PROFILE);
        }
        userProfile.scaleToFit(60, 60);

        final var cellUserProfile = new PdfPCell(userProfile);
        cellUserProfile.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellUserProfile.setVerticalAlignment(Element.ALIGN_CENTER);
        cellUserProfile.setBorder(Rectangle.NO_BORDER);
        cellUserProfile.setPaddingTop(20);
//        cellUserProfile.setPaddingBottom(16.2f);
        cellUserProfile.setFixedHeight(96.5f);


        final var cellCompanyNameKh = new PdfPCell(new Paragraph(kh.process("គណបក្សកម្ពុជានិយម"),
                new Font(bfComic, NORMAL_FONT, Font.BOLD, BaseColor.BLUE)));
        cellCompanyNameKh.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellCompanyNameKh.setBorder(Rectangle.NO_BORDER);

        final var cellCompanyNameEn = new PdfPCell(new Paragraph("Kampucheaniyum Party",
                new Font(Font.FontFamily.TIMES_ROMAN, NORMAL_FONT, Font.BOLD, BaseColor.BLUE)));
        cellCompanyNameEn.setHorizontalAlignment(Element.ALIGN_CENTER);
        cellCompanyNameEn.setBorder(Rectangle.NO_BORDER);
        cellCompanyNameEn.setPaddingTop(3);

        table1.addCell(cellLogo);
        table1.addCell(cellCompanyNameKh);
        table1.addCell(cellCompanyNameEn);

        table1.addCell(cellUserProfile);
        document.add(table1);
    }

    public byte[] getUserIdCard(final AppUser user) throws Exception {
        final var document = new Document();
        final var fileOutPutStream = new FileOutputStream(OUTPUT_FILE);
        document.setPageSize(new Rectangle(153.07056f, 243.77976f));
        document.setMargins(0, 0, 5, 0);
        PdfWriter.getInstance(document, fileOutPutStream);
        document.open();

        final var kh = new KhmerLigaturizer();
        header(document, kh, user);
        // qr code block
        PdfPTable tableButton = new PdfPTable(2);
        tableButton.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableButton.setWidthPercentage(100);
        tableButton.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableButton.setWidths(new int[]{6, 4});
        tableButton.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        PdfPTable tableText = new PdfPTable(1);
        tableButton.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableButton.setWidthPercentage(100);
        tableButton.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        // card number
        final var cellCardNumberTitle = new PdfPCell(new Paragraph(kh.process("លេខកាត"),
                new Font(bfComic, 5, Font.NORMAL, BaseColor.WHITE)));
        cellCardNumberTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellCardNumberTitle.setBorder(Rectangle.NO_BORDER);
        tableText.addCell(cellCardNumberTitle);

        final var cellCardNumber = new PdfPCell(new Paragraph(user.getCardNumber(),
                new Font(Font.FontFamily.TIMES_ROMAN, NORMAL_FONT, Font.BOLD, BaseColor.WHITE)));
        cellCardNumber.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellCardNumber.setPaddingBottom(5);
        cellCardNumber.setBorder(Rectangle.NO_BORDER);
        tableText.addCell(cellCardNumber);

        // name
        final var cellMemberNameTitle = new PdfPCell(new Paragraph(kh.process("ឈ្មេាះសមាជិក"),
                new Font(bfComic, 5, Font.NORMAL, BaseColor.WHITE)));
        cellMemberNameTitle.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellMemberNameTitle.setBorder(Rectangle.NO_BORDER);
        tableText.addCell(cellMemberNameTitle);

        final var cellMemberName = new PdfPCell(new Paragraph(kh.process(user.getDisplayName()),
                new Font(bfComic, NORMAL_FONT, Font.BOLD, BaseColor.WHITE)));
        cellMemberName.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellMemberName.setPaddingBottom(5);
        cellMemberName.setBorder(Rectangle.NO_BORDER);
        tableText.addCell(cellMemberName);

        // created date
        final var createdDay = dateUtilHelper.getNumberAsKhmer(String.valueOf(user.getCreatedAt().getDayOfMonth()));
        final var createdMonth = dateUtilHelper.getMonthAsKhmer(user.getCreatedAt().getMonthValue());
        final var createdYear = dateUtilHelper.getNumberAsKhmer(String.valueOf(user.getCreatedAt().getYear()));
        final var cellMemberCreatedDate = new PdfPCell(new Paragraph(kh.process("ថ្ងៃចុះឈ្មេាះ " + createdDay + " " + createdMonth + " " + createdYear),
                new Font(bfComic, 5, Font.NORMAL, BaseColor.WHITE)));
        cellMemberCreatedDate.setHorizontalAlignment(Element.ALIGN_LEFT);
        cellMemberCreatedDate.setBorder(Rectangle.NO_BORDER);
        tableText.addCell(cellMemberCreatedDate);

        tableButton.addCell(tableText);

        final var qrcode = new BarcodeQRCode(user.getQrCode(), 500, 500, null);
        Image qrcodeImage = qrcode.getImage();

        tableButton.addCell(qrcodeImage);

        PdfPTable tableButtonMaster = new PdfPTable(1);
        tableButtonMaster.setHorizontalAlignment(Element.ALIGN_CENTER);
        tableButtonMaster.setWidthPercentage(100);
        tableButtonMaster.setHorizontalAlignment(Element.ALIGN_LEFT);
        tableButtonMaster.setWidths(new int[]{10});
        tableButtonMaster.getDefaultCell().setBorder(Rectangle.NO_BORDER);

        PdfPCell cellMaster = new PdfPCell(tableButton);
        cellMaster.setBackgroundColor(new BaseColor(0, 101, 153));
        cellMaster.setPaddingTop(5);
        cellMaster.setPaddingBottom(5);
        cellMaster.setPaddingLeft(0);
        cellMaster.setPaddingRight(7);
        tableButtonMaster.addCell(cellMaster);

        document.add(tableButtonMaster);

        document.close();
        final var originalPath = Paths.get(OUTPUT_FILE);
        return Files.readAllBytes(originalPath);
    }
}
