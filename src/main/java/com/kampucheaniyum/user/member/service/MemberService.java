package com.kampucheaniyum.user.member.service;

import com.kampucheaniyum.basic.notification.service.NotificationService;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.user.member.domain.Member;
import com.kampucheaniyum.user.member.domain.MemberStatus;
import com.kampucheaniyum.user.member.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {

    private final MemberRepository repository;
    private final NotificationService notificationService;

    public void approve(final Long id) {
        repository.findById(id)
                .map(member -> member.setStatus(MemberStatus.APPROVED))
                .map(member -> {
                    notificationService.memberApproved(member);
                    return repository.save(member);
                })
                .orElseThrow(() -> new ResourceNotFoundException(Member.class, id));
    }

    public void reject(final Long id, final String reason) {
        repository.findById(id)
                .map(member -> member.setStatus(MemberStatus.REJECTED).setRejectReason(reason))
                .map(repository::save)
                .orElseThrow(() -> new ResourceNotFoundException(Member.class, id));
    }
}
