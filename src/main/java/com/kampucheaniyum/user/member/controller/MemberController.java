package com.kampucheaniyum.user.member.controller;

import com.kampucheaniyum.appconfiguration.utils.ApplicationSecurityContext;
import com.kampucheaniyum.exception.BadRequestException;
import com.kampucheaniyum.exception.ForbiddenException;
import com.kampucheaniyum.exception.ResourceNotFoundException;
import com.kampucheaniyum.exception.UnAuthorizeException;
import com.kampucheaniyum.persistence.service.EntityDataMapper;
import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.authentication.domain.UserRole;
import com.kampucheaniyum.user.authentication.repository.AppUserRepository;
import com.kampucheaniyum.user.member.data.MemberData;
import com.kampucheaniyum.user.member.data.mapper.MemberMapper;
import com.kampucheaniyum.user.member.domain.Member;
import com.kampucheaniyum.user.member.domain.MemberStatus;
import com.kampucheaniyum.user.member.repository.MemberRepository;
import com.kampucheaniyum.user.member.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.Map;
import java.util.Set;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/member")
public class MemberController {

    private static final Set<String> COMMAND = Set.of("APPROVE", "REJECT");

    private final MemberRepository repository;
    private final MemberMapper mapper;
    private final EntityDataMapper dataMapper;
    private final ApplicationSecurityContext context;
    private final MemberService service;
    private final AppUserRepository userRepository;

    @PostMapping
    public MemberData create(@Valid @RequestBody MemberData data) {
        return mapper.apply(repository.save(mapper.map(data)));
    }

    @PutMapping("{id}")
    public MemberData update(@PathVariable Long id, @RequestBody MemberData data) {
        return repository.findById(id)
                .map(member -> mapData(member, data))
                .map(mapper)
                .orElseThrow(() -> new ResourceNotFoundException(Member.class, id));
    }

    @PostMapping("{id}")
    @Transactional
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public void approve(@PathVariable Long id,
                        @RequestParam(defaultValue = "APPROVE") String command,
                        @RequestParam(required = false) String reason) {
        if (!COMMAND.contains(command)) {
            throw new BadRequestException("command can only be:" + COMMAND);
        }
        switch (command) {
            case "APPROVE":
                service.approve(id);
                break;
            case "REJECT":
                service.reject(id, reason);
        }
    }

    @GetMapping
    public MemberData get() {
        if (context.authenticatedUser() == null) {
            throw new UnAuthorizeException();
        } else if (context.hasPermission(UserRole.ADMIN.name()) || context.hasPermission(UserRole.EDITOR.name())) {
            throw new ForbiddenException("This API is for self user only please use /api/member/all for admin");
        } else {
            return repository.findFirstByCreatedBy(context.authenticatedUser())
                    .map(mapper)
                    .orElse(null);
        }
    }

    @GetMapping("/all")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public Page<MemberData> getAll(Pageable pageable, String status) {
        if (context.authenticatedUser() == null) {
            throw new UnAuthorizeException();
        }
        if (StringUtils.hasText(status)) {
            if ("APPROVED".equalsIgnoreCase(status)) {
                return repository.findAllByStatus(MemberStatus.APPROVED, pageable).map(mapper);
            } else if ("PENDING".equalsIgnoreCase(status)) {
                return repository.findAllByStatus(MemberStatus.REJECTED, pageable).map(mapper);
            }
        }
        return repository.findAll(pageable).map(mapper);
    }

    @GetMapping("{id}")
    public MemberData getByCreatedBy(@PathVariable("id") final Long createdId) {
        final var user = userRepository.findById(createdId)
                .orElseThrow(() -> new ResourceNotFoundException(AppUser.class, createdId));
        return repository.findFirstByCreatedBy(user)
                .map(mapper)
                .orElse(null);
    }

    @GetMapping("/statistic")
    @PreAuthorize("hasAnyAuthority('ADMIN', 'EDITOR')")
    public Map<String, Object> getStatistic() {
        return Map.of(
                "approved", repository.countByStatus(MemberStatus.APPROVED),
                "pending", repository.countByStatus(MemberStatus.PENDING),
                "rejected", repository.countByStatus(MemberStatus.REJECTED));
    }

    private Member mapData(Member member, MemberData data) {
        return dataMapper.mapObject(mapper.map(data), member, Member.class);
    }
}
