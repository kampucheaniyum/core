package com.kampucheaniyum.user.member.data;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.kampucheaniyum.persistence.data.AuditingEntityData;
import com.kampucheaniyum.user.member.domain.Address;
import com.kampucheaniyum.user.member.domain.MemberStatus;
import com.kampucheaniyum.user.member.domain.MemberType;
import com.kampucheaniyum.user.member.domain.Sex;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
public class MemberData extends AuditingEntityData {
    @NotNull
    private MemberType type;
    @NotBlank
    private String khmerFullName;
    private String englishFullName;
    @NotBlank
    private String idCardNo;
    private String idCardImageFront;
    private String idCardImageBack;
    @NotNull
    private Sex sex;
    @NotNull
    @JsonFormat(pattern = "yyyy-MM-dd")
    private LocalDate dateOfBirth;
    @NotNull
    private Address address;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private MemberStatus status;
    @JsonProperty(access = JsonProperty.Access.READ_ONLY)
    private String rejectReason;
}
