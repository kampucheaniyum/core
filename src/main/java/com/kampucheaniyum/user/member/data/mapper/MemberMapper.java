package com.kampucheaniyum.user.member.data.mapper;

import com.kampucheaniyum.persistence.mapper.DataMapper;
import com.kampucheaniyum.user.member.data.MemberData;
import com.kampucheaniyum.user.member.domain.Member;
import org.springframework.stereotype.Component;

@Component
public class MemberMapper extends DataMapper<Member, MemberData> {
}
