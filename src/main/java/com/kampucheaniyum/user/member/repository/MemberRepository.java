package com.kampucheaniyum.user.member.repository;

import com.kampucheaniyum.user.authentication.domain.AppUser;
import com.kampucheaniyum.user.member.domain.Member;
import com.kampucheaniyum.user.member.domain.MemberStatus;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface MemberRepository extends JpaRepository<Member, Long> {
    Optional<Member> findFirstByCreatedBy(final AppUser createdBy);

    Page<Member> findAllByStatus(MemberStatus status, Pageable pageable);

    long countByStatus(MemberStatus status);
}